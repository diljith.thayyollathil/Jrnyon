import React, { Component } from 'react';
import {View, Text} from 'react-native';
import reducers from './src/reducers';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware}  from 'redux';
import Router from './src/router';
import thunk from 'redux-thunk';


class App extends Component{
    render(){
        return(
            <Provider store={ createStore(reducers,applyMiddleware(thunk)) }>
                <Router />
            </Provider>
        )
    }
}

export default App;
