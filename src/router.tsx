import React from 'react';
import {AsyncStorage} from 'react-native';
import { Scene, Router } from 'react-native-router-flux';
import Login from './Components/Login';
import Itenrary_Timeline from './Components/Itnerary/itnerary_timeline';
import Invitees from './Components/Invitees';
import Home from './Components/home';
import TripView from './Components/TripFullView';
import TripViewInvited from './Components/tripFullViewInvited';
import Error from './Components/errorPage';
import BookingHome from './Components/booking';
import Gallery from './Components/Gallery';
import IndividualUploadings from './Components/Gallery/individualUploaded';
import AllUpcoming from './Components/allUpcoming';

class RouterComponent extends React.Component{
    constructor(){
        super();
        this.state = {
            initialPage:true,
        }
    }
    componentDidMount(){
        let data = AsyncStorage.getItem('login');
    }
    render()
    {
        return (
            <Router>
                <Scene key='root'>
                    <Scene key='login' component={Login} hideNavBar={true}   ></Scene>
                    <Scene key='itnerary_timeline' component={Itenrary_Timeline} hideNavBar={true} ></Scene>
                    <Scene key='invitees' hideNavBar={true} component={Invitees} ></Scene>
                    <Scene key='home' hideNavBar={true} component={Home}  initial></Scene>
                    <Scene key='booking' hideNavBar={true} component={BookingHome} ></Scene>
                    <Scene key='tripDetails' hideNavBar={true} component={TripView}></Scene>
                    <Scene key='tripDetails_invited' hideNavBar={true} component={TripViewInvited}></Scene>
                    <Scene key='errorPage' hideNavBar={true} component={Error}></Scene>
                    <Scene key='galleryIdividual' hideNavBar={true} component={IndividualUploadings}></Scene>
                    <Scene key='gallery' hideNavBar={true} component={Gallery} ></Scene>
                    <Scene key='all_upcoming' hideNavBar={true} component={AllUpcoming}  ></Scene>
                </Scene>
            </Router>
        )
    }    
}
export default RouterComponent;