import {combineReducers} from 'redux';
import AuthReducer from './authreducer';
import ItenarariesReducer from './itnerariesReducer';
import InviteesReducer from './inviteesReducer';
import MytripsReducer from './mytripsReducer';
import AllTripsReducer from './alltripsReducer';
import IndividualTripReducer from './individualTrip';
import EnquireReducer from './enquiresReducer';
import FavouriteReducer from './favouritesReducer';
import LoginReducer from './loginReducer';
import UpcomingReducer from './upcomingReducer';
import CurrentTripReducer from './currentTripreducer';
import GalleryReducer from './galleryReducer';
import IPADRESS from './ipAdress';
import Booking from './bookingReducer';

export default combineReducers({
    auth : AuthReducer,
    itenarary: ItenarariesReducer,
    invitees:InviteesReducer,
    myTrips: MytripsReducer,
    allTrips:AllTripsReducer,
    selected:IndividualTripReducer,
    enquires:EnquireReducer,
    favourite:FavouriteReducer,
    Login:LoginReducer,
    upComing:UpcomingReducer,
    current:CurrentTripReducer,
    Gallery:GalleryReducer,
    ipadress:IPADRESS,
    Booking:Booking,
})