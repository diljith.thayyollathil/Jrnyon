
import { FETCH_BOOKING_INFO_BEGIN, FETCH_BOOKING_INFO_FAILURE, FETCH_BOOKING_INFO_SUCCESS} from '../actions/types';

const INITIAL_STATE = {
    trips:[
    ]
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_BOOKING_INFO_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_BOOKING_INFO_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_BOOKING_INFO_SUCCESS:{
            return {bookingData:action.data}
        }
       
        default: return state;
    }
}