
import { LOGIN_DETAILS} from '../actions/types';

const INITIAL_STATE = {
    username:'',
    phonenumber: ''
}


export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case LOGIN_DETAILS : return {...state , username: action.payload.username, phonenumber: action.payload.phonenumber}
        default: return state;
    }
}