
import { FETCH_MYTRIP_BEGIN, FETCH_MYTRIP_FAILURE, FETCH_MYTRIP_SUCCESS} from '../actions/types';

const INITIAL_STATE = {
    trips:[
    ]
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_MYTRIP_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_MYTRIP_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_MYTRIP_SUCCESS:{
            return {trips:action.data}
        }
       
        default: return state;
    }
}