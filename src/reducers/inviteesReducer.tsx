
import { FETCH_INVITEES, ADD_INVITEES, FETCH_INVITEES_BEGIN,
    FETCH_INVITEES_FAILURE, FETCH_INVITEES_SUCCESS, ADDINVITEES_FAILURE, ADD_TO_INVITEE_BEGIN} from '../actions/types';

const INITIAL_STATE = {
    invitees:[
    ]
}
export default (state = INITIAL_STATE, action) => {
    console.log("inside reducer",action)
    switch(action.type){
        case ADD_INVITEES: {
            let data = [];
            state.invitees.map((item,i)=>{
                item['dp'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMjO9lsBeDLzlm9780HyMXWa0c07DnpzPy_CCYoc3kqX85WGh4Mw';
                data.push(item)
            });
            let invite =action.data.inviteeData
            return {
               invitees:[...data,...invite]
            }
        }
        case FETCH_INVITEES_BEGIN: {
            return {
                ...state, loading:true, error:false
            }
        }
        case FETCH_INVITEES_FAILURE: {
            return {
                ...state, error:true,loading:false
            }
        }
        case FETCH_INVITEES_SUCCESS: {
            return{
                invitees:action.data, loading:false, error:false
            }
        }


        case ADDINVITEES_FAILURE:{
            return {
                error:true,loading:false
            }
        }
        case ADD_TO_INVITEE_BEGIN : {
            return {
                ...state,error:false,loading:true
            }
        }
        default: return state;
    }
}