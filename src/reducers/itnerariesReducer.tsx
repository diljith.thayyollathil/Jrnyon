
import { FETCH_ITENERARIES, FETCH_ITENERIES_FAILURE, FETCH_ITENERIES_BEGIN} from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = {
    itneraries:[]
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_ITENERARIES : {
            return {...state,itneraries:action.data,error:false,loading:false}
        }
        case FETCH_ITENERIES_FAILURE:{
            return {...state,error:true,loading:false}
        }
        case FETCH_ITENERIES_BEGIN:{
            return {...state, error:false,loading:true}
        }
        default: return state;
    }
}
