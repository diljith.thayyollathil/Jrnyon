
import { FETCH_ALL_UPCOMING_BEGIN, FETCH_ALL_UPCOMING_FAILURE,
     FETCH_ALL_UPCOMING_SUCCESS, FETCH_SINGLE_UPCOMING_BEGIN, FETCH_SINGLE_UPCOMING_FAILURE,
     FETCH_SINGLE_UPCOMING_SUCCESS} from '../actions/types';

const INITIAL_STATE = {
    trip:[
    ],
    trips:[
    ]
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_ALL_UPCOMING_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_ALL_UPCOMING_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_ALL_UPCOMING_SUCCESS:{
            return {trips:action.data}
        }
        case FETCH_SINGLE_UPCOMING_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_SINGLE_UPCOMING_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_SINGLE_UPCOMING_SUCCESS:{
            return {trip:action.data}
        }
       
        default: return state;
    }
}