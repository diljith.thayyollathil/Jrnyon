
import { FETCH_CURENT_TRIP_BEGIN, FETCH_CURENT_TRIP_FAILURE, FETCH_CURENT_TRIP_SUCCESS} from '../actions/types';

const INITIAL_STATE = {
    trips:[
    ]
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_CURENT_TRIP_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_CURENT_TRIP_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_CURENT_TRIP_SUCCESS:{
            return {trips:action.data}
        }
       
        default: return state;
    }
}