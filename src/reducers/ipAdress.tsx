
import {SET_IP_ADDRESS} from '../actions/types';

const INITIAL_STATE = {
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case SET_IP_ADDRESS:{
            return {...state,ipAdress:action.data}
        }
        default: return state;
    }
}

