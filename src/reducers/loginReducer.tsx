
import { LOGIN_FAILURE, LOGIN_SUCCESS, RESET_LOGIN_ATTEMPT} from '../actions/types';

const INITIAL_STATE = {
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case LOGIN_FAILURE:{
            return {loading:false,error:true,loginAttempt:false}
        }
        case LOGIN_SUCCESS:{
            return {loading:false,error:true, loginAttempt:true}
        }
        case RESET_LOGIN_ATTEMPT:{
            return {loading:false,error:false, loginAttempt:false}
        }
        default: return state;
    }
}