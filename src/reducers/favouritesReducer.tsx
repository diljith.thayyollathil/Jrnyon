
import {FETCH_FAVOURITES_BEGIN, FETCH_FAVOURITES_FAILURE, FETCH_FAVOURITES_SUCCESS,
     ADD_TO_FAVOURITE_STORE, REMOVE_FROM_FAVOURITES } from '../actions/types';

const INITIAL_STATE = {
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_FAVOURITES_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_FAVOURITES_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_FAVOURITES_SUCCESS : {
            return {trips:action.data}
        }
        case ADD_TO_FAVOURITE_STORE : {
            return {trips:[...state.trips,action.data]}
        }
        case REMOVE_FROM_FAVOURITES : {
            let newData = state.trips.filter((item)=>{
                return item.id!=action.data.id
            });
            return {
                ...state,trips:newData
            }
        }
        default: return state;
    }
}