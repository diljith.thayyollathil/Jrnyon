
import {FETCH_ENQUIRIES_BEGIN, FETCH_ENQUIRIES_FAILURE, FETCH_ENQUIRIES_SUCCESS, ADD_TO_ENQUIRE_LIST } from '../actions/types';

const INITIAL_STATE = {
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_ENQUIRIES_BEGIN:{
            return {...state,loading:true,error:false,trips:[]}
        }
        case FETCH_ENQUIRIES_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_ENQUIRIES_SUCCESS : {
            return {trips:action.data}
        }
        case ADD_TO_ENQUIRE_LIST : {
            return {trips:[...state.trips,action.data]}
        }
       
        default: return state;
    }
}