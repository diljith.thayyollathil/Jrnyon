import {FETCH_SINGLETRIP_BEGIN,
    FETCH_SINGLETRIP_FAILURE,FETCH_SINGLETRIP_SUCCESS, JOIN_TRIP_SUCCESS,
     JOIN_TRIP_BEGIN, JOIN_TRIP_FAILURE, REJECT_TRIP_SUCCESS, REJECT_TRIP_FAILURE, REJECT_TRIP_BEGIN} from '../actions/types';

const INITIAL_STATE = {
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_SINGLETRIP_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_SINGLETRIP_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_SINGLETRIP_SUCCESS:{
            return {trip:action.data}
        }
        case JOIN_TRIP_SUCCESS :{
            return {...state,loading:false,error:false,has_joined:true}
        }
        case JOIN_TRIP_BEGIN :{
            return {...state,loading:true,error:false, has_joined:false}
        }
        case JOIN_TRIP_FAILURE :{
            return {...state,loading:false,error:true, has_joined:false}
        }
        case REJECT_TRIP_SUCCESS :{
            return {...state,loading:false,error:false,has_rejected:true}
        }
        case REJECT_TRIP_BEGIN :{
            return {...state,loading:true,error:false, has_rejected:false}
        }
        case REJECT_TRIP_FAILURE :{
            return {...state,loading:false,error:true, has_rejected:false}
        }
        default: return state;
    }
}