
import { FETCH_GALLERY_BEGIN, FETCH_GALLERY_FAILURE, FETCH_GALLERY_SUCCESS,
    GALLERY_INDIVIDUAL_SUCCESS, GALLERY_INDIVIDUAL_FAILURE, 
    GALLERY_INDIVIDUAL_BEGIN, SET_GALLERY_ID, FETCH_GALLERY_ID} from '../actions/types';

const INITIAL_STATE = {
    trips:[
    ],
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_GALLERY_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case FETCH_GALLERY_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_GALLERY_SUCCESS:{
            return {...state,trips:action.data}
        }
        case GALLERY_INDIVIDUAL_BEGIN:{
            return {...state,loading:true,error:false}
        }
        case GALLERY_INDIVIDUAL_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case GALLERY_INDIVIDUAL_SUCCESS:{
            return {...state,individualGallery:action.data}
        }
        case SET_GALLERY_ID :{
            return {id:action.data.id}
        }
        case FETCH_GALLERY_ID:{
            return {...state}
        }
       
        default: return state;
    }
}