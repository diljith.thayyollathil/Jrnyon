
import { FETCH_ALLTRIP_BEGIN, FETCH_ALLTRIP_FAILURE, 
    FETCH_ALLTRIP_SUCCESS, ADD_TO_FAVOURITES_BEGIN, 
    ADD_TO_FAVOURITES_FAILURE, ADD_TO_FAVOURITES_SUCCESS, REMOVE_ENQUIRED, SET_IP_ADDRESS} from '../actions/types';

const INITIAL_STATE = {
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case FETCH_ALLTRIP_BEGIN:{
            return {...state,loading:true,error:false,trips:[]}
        }
        case FETCH_ALLTRIP_FAILURE:{
            return {...state,loading:false,error:true}
        }
        case FETCH_ALLTRIP_SUCCESS : {
            return {trips:action.data}
        }
        case ADD_TO_FAVOURITES_BEGIN :{
            return {...state,loading:true,error:false}
        }
        case ADD_TO_FAVOURITES_FAILURE :{
            return {...state,loading:false,error:true}
        }
        case ADD_TO_FAVOURITES_SUCCESS :{
            return {...state,loading:false,error:false}
        }
        case REMOVE_ENQUIRED:{
            const tripsFiltered = state.trips.filter((item)=>{
                return item.id != action.data;
            })
            return {...state,trips:tripsFiltered}
        }
        default: return state;
    }
}