declare interface Trip {
    amount:string;
    id:number;
    images:string[];
    name:string;
    rating:number;
    tags:string[];
}