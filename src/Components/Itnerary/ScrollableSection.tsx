import React from 'react';
import {View,Image,Text, ScrollView} from 'react-native';
import ScrollableIndividual from './scrollableIndividual';

class ScrollableSection extends React.Component{
    render(){
        const {iconStyles, heading} = styles;
        let url=''
        if(this.props.title=='Stay'){
            url = require('../resourses/img/hotel1.png');
        }
        if(this.props.title=='Food'){
            url = require('../resourses/img/food.png');
        }
        if(this.props.title=='Activities'){
            url = require('../resourses/img/activity.png');
        }
        return(
            <View >
                <View style={{marginBottom:20,marginTop:20}}>
                    <Image source={url} style={iconStyles} />
                    <Text style={heading}>{this.props.title}</Text>
                </View>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                    {this.props.data!=undefined && this.props.data.map((item,i)=>{
                        return(
                            <ScrollableIndividual ip={this.props.ip} openDetailsPop={this.props.openDetailsPop} title={this.props.title} data={item} key={i}/>
                        )
                    })}
                </ScrollView>
               
            </View>
        )
    }
}
const styles = {
    iconStyles:{
        height:35,width:35,
        position:'absolute',
        top:-10
    },
    heading:{
        fontWeight:'800',
        marginLeft:38
    }
}
export default ScrollableSection;