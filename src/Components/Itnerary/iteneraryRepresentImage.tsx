import React from 'react';
import {Image, View, Dimensions} from 'react-native';

class RepresentImage extends React.Component{
   
    render(){
        let {locationStyle} = styles;
        return(
            <View>
                <Image style={locationStyle} source={require('../resourses/img/represent.jpg')}/>
            </View>
        )
    }
}

const win = Dimensions.get('window');

const styles={
    locationStyle:{
        flex: 1,
        alignSelf: 'stretch',
        width: win.width,
        height: 150,
        marginTop:10,
        marginBottom:10
    }
}
export default RepresentImage;