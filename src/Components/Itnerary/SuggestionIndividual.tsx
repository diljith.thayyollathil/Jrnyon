import React from 'react';
import {View, Image, Text} from 'react-native';
export interface Suggestionprops{
    data:string;
}
export interface Suggestionstate{
    data:string;
}

class SuggestionIndividual extends React.Component<Suggestionprops, Suggestionstate>{
    constructor(props: Suggestionprops){
        super(props);
    }
    render(){
        let {suggestionContainer} = styles;
        return(
            <View style={suggestionContainer}>
                <View style={{flex:1,width:165,alignSelf:'flex-start',alignItems:'flex-start',paddingLeft:15}}>
                    <View style={{alignItems:'center',width:120,backgroundColor:'#ededed',borderRadius:14,height:28,marginTop:8,paddingTop:6,borderColor:'#eaeaea',marginLeft:-5}}>
                        <Text style={{textAlign:'center',fontSize:12,fontWeight:'bold'}}>{this.props.data}</Text> 
                    </View>
                    <View style={{alignItems:'flex-start',width:120,borderRadius:14,height:17,marginTop:5,paddingTop:3}}>
                        <Text style={{textAlign:'center',fontSize:12,fontWeight:'bold'}}>Restaurent Name</Text> 
                    </View>
                    <View style={{alignItems:'flex-start',width:120,borderRadius:14,height:15,marginTop:12}}>
                        <Text style={{textAlign:'center',fontSize:12}}>Street, Location</Text> 
                    </View>
                    <View style={{alignItems:'flex-start',width:120,height:15,marginTop:10,flexDirection:"row",justifyContent:'space-between'}}>
                        <View style={{width:59}}>
                            <Text style={{fontSize:11,color:'#ffaa5d'}}>LOCATION</Text> 
                        </View>
                        <View style={{width:59,alignItems:'flex-end'}}>
                            <Text style={{fontSize:11,color:'#ffaa5d'}}>DETAILS</Text> 
                        </View>
                    </View>
                </View>
                <Image style={{width:100,height:130,alignSelf:'flex-end'}} source={require('../resourses/img/represent.jpg')} />
            </View>
        )
    }
}
const styles = {
    suggestionContainer:{
        backgroundColor:'#fff',
        shadowColor: '#ccc',
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        height:130,
        width:265,
        marginRight:20,
        borderRadius:5,
        marginTop:10,
        marginBottom:10
    }
}
export default SuggestionIndividual;