import React from 'react';
import { View, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export interface InclusionProps {
    inclusion:{
        inclusionName:String;
        reprecentativeImage:String;
    };
}
export interface InclusionState {
    
}
class InclusionIndividual extends React.Component<InclusionProps,InclusionState> {
    constructor(props:InclusionProps ){
        super(props);
        this.state={

        }
    }
    render() {
        
        let tags = ["","","",""];
        let inclusions = this.props.inclusion;
        return (
            <View style={{ padding: 20, width: 70, backgroundColor: '#fff',paddingRight:0}}>
                <View style={{ borderRadius: 25, borderWidth: 1,  
                padding: 10, backgroundColor: '#fff',borderColor:'#ccc' }}>
                    {inclusions=='transport' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/transfers.png')}/>}
                    {inclusions=='breakfast' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/breakfast.png')}/>}
                    {inclusions=='stay' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/hotel.png')}/>}
                    {inclusions=='lunch' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/dinner_lunch.png')}/>}
                    {inclusions=='dinner' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/dinner_lunch.png')}/>}
                    {inclusions =='sightseeing' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/sightseeing.png')}/>} 
                    {(inclusions !='sightseeing' && inclusions!='dinner' && inclusions!="lunch" && inclusions!="stay" &&
                inclusions!="breakfast" && inclusions!="transport") && <Image style={{height:27,width:27}} source={require('../resourses/img/inclusion_dummy.png')}/>}
                    
                </View>
                <Text style={{color:'#767676',fontSize:11,textAlign:'center'}}>{inclusions.charAt(0).toUpperCase() + inclusions.slice(1)}</Text>
            </View>
        )
    }
}
export default InclusionIndividual;