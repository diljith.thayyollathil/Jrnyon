import React from 'react';
import { View, FlatList, Text, ScrollView } from 'react-native';
import InclusionIndividual from './inclusionsIndividual';

class Inclusions extends React.Component {
    render() {
        return (
            <View style={{ height: 100}}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                    {this.props.inclusions.map((item,i)=>{
                        return(
                            <InclusionIndividual inclusion={item} key={i}/>
                        )
                    })}
                    
                </ScrollView>
            </View>
        )
    }
}
export default Inclusions;
