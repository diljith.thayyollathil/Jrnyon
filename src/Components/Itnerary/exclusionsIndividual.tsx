import React from 'react';
import { View, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export interface InclusionProps {
    inclusion:{
        inclusionName:String;
        reprecentativeImage:String;
    };
}
export interface InclusionState {
    
}
class ExclusionIndividual extends React.Component<InclusionProps,InclusionState> {
    constructor(props:InclusionProps ){
        super(props);
        this.state={

        }
    }
    render() {
        let exclusion = this.props.exclusion;
        return (
            <View style={{padding: 20, width: 70, backgroundColor: '#fff',paddingRight:0}}>
                <View style={{ borderRadius: 25, borderWidth: 1,  
                padding: 10, backgroundColor: '#fff',borderColor:'#ccc' }}>
                     {exclusion=='transport' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/transfers.png')}/>}
                    {exclusion=='breakfast' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/breakfast.png')}/>}
                    {exclusion=='stay' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/hotel.png')}/>}
                    {exclusion=='lunch' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/dinner_lunch.png')}/>}
                    {exclusion=='dinner' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/dinner_lunch.png')}/>}
                    {exclusion =='sightseeing' &&  <Image style={{height:27,width:27}} source={require('../resourses/img/sightseeing.png')}/>} 
                    {(exclusion !='sightseeing' && exclusion!='dinner' && exclusion!="lunch" && exclusion!="stay" &&
                exclusion!="breakfast" && exclusion!="transport") && <Image style={{height:27,width:27}} source={require('../resourses/img/exclusion_dummy.png')}/>}
                </View>
                <Text style={{color:'#767676',fontSize:11,textAlign:'center'}}>{exclusion.charAt(0).toUpperCase() + exclusion.slice(1)}</Text>
            </View>
        )
    }
}
export default ExclusionIndividual;