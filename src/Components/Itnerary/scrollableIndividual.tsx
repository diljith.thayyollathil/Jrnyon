import React from 'react';
import {View,Image,Text, TouchableOpacity} from 'react-native';

class ScrollableIndividual extends React.Component{
    render(){
        const defaultPic = "";
        if(this.props.data.image!=""){
            defaultPic =this.props.ip+"/media/"+this.props.data.image;
        }else{
            defaultPic = "../resourses/img/dummy_image.png";
        }
       
        const {container, wrapper} = styles;
        return(
            <View style={container}>
                <View style={wrapper}>
                    <View style={{alignSelf:'flex-end',marginLeft:110}}>
                        <View style={{alignItems:'flex-start',width:120,borderRadius:14,height:15,marginTop:12}}>
                            <Text style={{textAlign:'center',fontSize:12}}>{this.props.title=='Activities'?this.props.data.name:this.props.data.place}</Text> 
                        </View>
                        <View style={{alignItems:'flex-start',width:120,borderRadius:14,height:15}}>
                            <Text style={{textAlign:'center',fontSize:12}}>{this.props.title=='Activities'?this.props.data.location:this.props.data.address}</Text> 
                        </View>
                        <View style={{alignItems:'flex-start',width:120,height:15,marginTop:10,flexDirection:"row",justifyContent:'space-between'}}>
                            <View style={{width:59}}>
                                <TouchableOpacity onPress={this.props.openDetailsPop.bind(this,{description:this.props.data.description,title:this.props.title=='Activities'?this.props.data.name:this.props.data.place})}>
                                    <Text style={{fontSize:11,color:'#ffaa5d'}}>DETAILS</Text> 
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <Image style={{position:'absolute',left:10,width:100,height:70,alignSelf:'flex-start'}} source={{uri : defaultPic}}/>

                </View>
            </View>
        )
    }
}
const styles = {
    container:{
        
    },
    wrapper:{
        width:250,
        height:70
    }
}
export default ScrollableIndividual;