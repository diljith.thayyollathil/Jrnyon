import React from 'react';
import { View, Text, TouchableOpacity, Animated, StyleSheet } from 'react-native';
import ActiveItenarary from './activeItenarary';
import NormalItnerary from './normalItenarary';
import ItneraryDetails from './itenararyDetails';
import LocateText from './locateOnMap';
import { SubHead } from '../common';
import Inclusions from './inclusions';
import Exclusions from './exclusions';
import SuggestionList from './suggestionList';
import ScrollableSection from './ScrollableSection';


export interface activityList {
    id: number;
    name: string;
    desc: string;
    is_new: boolean;
}
export interface itneraryList {
    title: string;
    date: string;
    metaInfo: string;
    description: string;
    day_count: number;
    activities: activityList[];
}


export interface IteneraryRowProps {
    item: itneraryList;
    itemCount: number;
    closeAllRows: () => any;
    showHider:boolean;
    openDetailsPop:()=> any;
}
export interface IteneraryRowState {
    showInfo: boolean;
    rowStyle: {};
    DetailsStyle: {};
    activeTab: {};
    
}

export default class IteneraryRow extends React.Component<IteneraryRowProps, IteneraryRowState> {
    constructor(props: IteneraryRowProps) {
        super(props);
        this.state = {
            showInfo: false,
            rowStyle: {},
            DetailsStyle: {},
            activeTab: {},
           

        }
        
    }
    async itenararyRowClick() {
        let showInfo = this.state.showInfo;
        await this.setState({ showInfo: !showInfo });
        this.props.closeAllRows();
        if (this.state.showInfo) {
            this.setState({ rowStyle: { borderTopLeftRadius: 20, borderTopRightRadius: 20 } });
            this.setState({ DetailsStyle: { height: undefined } });
            this.setState({ activeTab: { backgroundColor: '#fff', borderWidth: 1, borderColor: '#fff', borderBottomColor: '#ececec', borderTopColor: '#ececec', } });
        } else {
            this.setState({ rowStyle: {} });
            this.setState({ DetailsStyle: {} });
            this.setState({ activeTab: {} });
        }
    }
   
    render() {
        const { rowContainer,rowContainerAvoided, rowBody, itemDateTimeContainer, dayIndicator,
            dateIndicator, timelineTailHider, itemDetailsContainer, Capsule,
            capsuleHead, rowInformationContainer, equalipadding, timelineTailHiderWhite } = styles;
        let { date, day_count, title, activities, description, metaInfo , map_pointes, bed,inclusions, activities,food, free_activities} = this.props.item;
        let timelineMask = 'timelineTailHider';
        const boxshadowstyle = this.props.showHider ? { shadowColor: '#ccc',
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                    shadowRadius: 1,
                    shadowOpacity: 0.7}:{};
        return (
            <View style={[{ paddingLeft: 18, backgroundColor: '#f4f4f4' }, this.state.activeTab,boxshadowstyle]}>
                {this.state.showInfo ? <ActiveItenarary /> : <NormalItnerary />}
                <View style={(this.props.showHider == true && this.state.showInfo)? rowContainerAvoided:rowContainer}>
                    {this.props.itemCount === 0 && <View>
                        {this.state.showInfo ? <View style={timelineTailHiderWhite}></View> : <View style={timelineTailHider}></View>}
                    </View>}
                    {this.props.showHider == true && <View>
                        {this.state.showInfo ? <View style={{position:'absolute',top:40,left:-10,width:20,height:70,
                    backgroundColor:'rgba(255,255,255,0)'}}></View> :<View style={{position:'absolute',top:40,left:-10,width:20,height:70,
                    backgroundColor:'#f4f4f4'}}></View>}
                    </View>}
                    <View style={{ paddingRight: 20 }}>
                        <TouchableOpacity onPress={this.itenararyRowClick.bind(this)}>
                            <View style={[rowBody, this.state.rowStyle]}>
                                <View style={itemDateTimeContainer}>
                                    <Text style={dateIndicator}>Day {day_count}</Text>
                                    <Text style={[dayIndicator]}>{date}</Text>
                                </View>
                                <View style={itemDetailsContainer}>
                                    <Text style={capsuleHead}>{title}</Text>
                                    <Text style={{ color: '#767676', fontSize: 12 }}>{metaInfo != "" ? ('(' + metaInfo + ')') : ""}</Text>
                                    {!this.state.showInfo && <View style={{ width: 55, height: 5, backgroundColor: '#fdac4c',borderRadius:10, marginTop: 10 }}></View>}
                                </View>
                            </View>
                        </TouchableOpacity>
                        {this.state.showInfo && <Animated.View style={[rowInformationContainer, this.state.DetailsStyle]}>
                            <View style={[{ flex: 1, marginBottom: 10 }, equalipadding]}>
                                <Text numberOfLines={3}>{description} </Text>
                            </View>
                            <View style={{ flex: 1, paddingLeft: 20 }}>
                                <LocateText locateMap={map_pointes} />
                            </View>
                            <View style={{padding:20,paddingLeft:0}}>
                                {bed!=undefined && bed.length!=0 && <View style={{ borderTopWidth: 1, borderTopColor: '#efefef' }}>
                                    <ScrollableSection ip={this.props.ip} openDetailsPop={this.props.openDetailsPop} title='Stay' data={bed} />
                                </View>}
                                {activities!=undefined && activities.length!=0 && <View style={{ borderTopWidth: 1, borderTopColor: '#efefef', marginTop: 30 }}>
                                    <ScrollableSection ip={this.props.ip} openDetailsPop={this.props.openDetailsPop} title='Activities' data={activities}/>
                                </View>}
                                {food!=undefined && food.length!=0 && <View style={{ borderTopWidth: 1, borderTopColor: '#efefef', marginTop: 30 }}>
                                    <ScrollableSection ip={this.props.ip} openDetailsPop={this.props.openDetailsPop} title='Food' data={food} />
                                </View>}
                                
                            </View>
                            {free_activities!=undefined && free_activities.length!=0 && <ItneraryDetails open_Details_AdditionalActivity={this.props.open_Details_AdditionalActivity} ip={this.props.ip} newEvent={free_activities} />}
                            <View>
                                <SubHead>Inclusions</SubHead>
                            </View>
                            <View style={{ borderColor: '#fff', borderBottomColor: "#efefef", borderWidth: 1, marginLeft: 14 }}>
                                <Inclusions inclusions={inclusions} />
                            </View>
                            <View style={{ marginTop: 30 }}>
                                <SubHead>Exclusions</SubHead>
                            </View>
                            <View style={{ borderColor: '#fff', borderBottomColor: "#efefef", borderWidth: 1, marginLeft: 14 }}>
                                <Exclusions exclusions={this.props.item.exclusions} />
                            </View>
                            <View style={{ marginTop: 10, marginBottom: 10 }}>
                                <SubHead>Suggestions</SubHead>
                            </View>
                            <View>
                                <SuggestionList />
                            </View>
                        </Animated.View>}
                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        borderStyle: 'solid',
        borderLeftWidth: 2,
        borderLeftColor: '#ccc',
        position: 'relative',
        elevation: 0,
        zIndex: 0,
        overflow: 'visible',
    },
    rowContainerAvoided:{
        borderStyle: 'solid',
        borderLeftWidth: 2,
        borderLeftColor: '#fff',
        position: 'relative',
        elevation: 0,
        zIndex: 0,
        overflow: 'visible',
    },
    rowBody: {
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 25
    },
    itemDateTimeContainer: {
        width: '30%',
        padding: 25,
        paddingRight: 10,
    },
    dayIndicator: {
        color: 'orange',
        fontSize: 13,
        fontWeight: 'bold',
    },
    dateIndicator: {
        fontSize: 16
    },
    timelineTailHider: {
        height: 30,
        width: 10,
        position: 'absolute',
        left: -5,
        backgroundColor: '#efefef'
    },
    timelineTailHiderWhite: {
        height: 30,
        width: 10,
        position: 'absolute',
        left: -5,
        backgroundColor: '#fff'
    },
    itemDetailsContainer: {
        width: '80%',
        paddingLeft: 0,
        paddingTop: 25,
        alignItems: 'flex-start',
    },
    capsuleHead: {
        fontFamily: 'Merriweather-Italic',
        fontSize: 16,
        textAlign: 'left',
        paddingLeft: 0
    },
    rowInformationContainer: {
        height: 100,
        flex: 1,
        marginTop: -30,
        marginBottom: 20,
        paddingTop: 0,
        borderColor: '#fff',
    },
    equalipadding: {
        paddingLeft: 20,
        paddingRight: 20
    }
});