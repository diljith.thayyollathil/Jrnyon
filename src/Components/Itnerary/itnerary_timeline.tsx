import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import IteneraryRow from './iteneryRows';

import { Icon } from 'react-native-material-ui';

import {
  Animated,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity
} from 'react-native';

import { CustomHeader1 } from '../common';
export interface timelineProps {

}
export interface activityList {
  id: number;
  name: string;
  desc: string;
  is_new: boolean;
}
export interface itneraryList {
  title: string;
  date: string;
  description: string;
  day_count: number;
  activities: activityList[];
}
export interface timelineState {
  scrollY: object;
  itenarary: itneraryList[];
  openDetailsPopup: boolean;
  detailsData: string;
  DescriptionAbout: string;
}

class Itenrary_Timeline extends Component<timelineProps, timelineState> {
  constructor(props: timelineProps) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      itenarary: [],
      openDetailsPopup: false,
      detailsData: '',
      DescriptionAbout: '',
      openDetailDescription: false,
      detailDescription: ''
    };
    this.openDetailsPop = this.openDetailsPop.bind(this);
  }
  open_Details_AdditionalActivity(data) {
    this.setState({ openDetailDescription: true });
    this.setState({ detailDescription: data });
  }

  componentDidMount() {
    let id = this.props.id;
    this.props.actions.fetch_itneraries(id);

  }
  openDetailsPop(data) {
    this.setState({ openDetailsPopup: true });
    this.setState({ detailsData: data.description });
    this.setState({ DescriptionAbout: data.title })
  }
  closeAllRows() {
    console.log("close all clicked")
  }
  _renderScrollViewContent() {
    const itenararyLength = this.props.itnery.itneraries.length;
    return (
      <View style={styles.scrollViewContent}>
        {this.props.itnery.itneraries.map((_item, i) => {
          return <IteneraryRow open_Details_AdditionalActivity={this.open_Details_AdditionalActivity.bind(this)} ip={this.props.ip} openDetailsPop={this.openDetailsPop.bind(this)} showHider={(i + 1) == itenararyLength ? true : false} itenararyLength={itenararyLength} closeAllRows={this.closeAllRows.bind(this)} item={_item} itemCount={i} key={i} />
        }
        )}
      </View>
    );
  }

  render() {
    const { fill } = styles;
    const HEADER_SCROLL_DISTANCE = 150 - 80;

    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [150, 80],
      extrapolate: 'clamp',
    });

    const floatingPosition = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [131, 69],
      extrapolate: 'clamp',
    });

    const backSize = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [35, 25],
      extrapolate: 'clamp',
    });

    const backPosition = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [65, 0],
      extrapolate: 'clamp',
    });
    const Titleposition = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [20, -30],
      extrapolate: 'clamp',
    });
    const TitleSize = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [20, 18],
      extrapolate: 'clamp',
    });
    const TitleHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [38, 30],
      extrapolate: 'clamp',
    });
    const TripcodeMargin = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [5, 0],
      extrapolate: 'clamp',
    });
    const TripcodeFontSize = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [14, 10],
      extrapolate: 'clamp',
    });
    const TagCapsuleHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [32, 28],
      extrapolate: 'clamp',
    });
    const TagCapsulePadding = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [7, 3],
      extrapolate: 'clamp',
    });
    const TagCapsuleFOntSIze = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [12, 10],
      extrapolate: 'clamp',
    });


    const activeDay = new Date(Date.parse('May 15 2018'));

    return (
      <View style={styles.fill}>
        <ScrollView
          style={styles.fill}
          scrollEventThrottle={16}
          onScroll={
            Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
          }
        >
          {this._renderScrollViewContent()}
        </ScrollView>
        <CustomHeader1 headerHeight={headerHeight}
          backPosition={backPosition}
          backSize={backSize}
          HeaderTitle='Itinerary'
          Titleposition={Titleposition}
          TitleSize={TitleSize}
          TitleHeight={TitleHeight}
          TripcodeMargin={TripcodeMargin}
          TripcodeFontSize={TripcodeFontSize}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.openDetailsPopup}
          onRequestClose={() => { this.setState({ openDetailsPopup: false }) }}>
          <View style={styles.containerStyle}>
            <View style={styles.CardSectionStyle}>
              <TouchableOpacity onPress={() => { this.setState({ openDetailsPopup: false }) }}>
                <Icon size={20} name='close' style={{ alignSelf: 'flex-end', marginTop: 5, marginBottom: 10, marginRight: 5, color: '#000' }} />
              </TouchableOpacity>
              <Text style={{ fontSize: 16, fontWeight: '800', marginBottom: 10 }}>{this.state.DescriptionAbout}</Text>
              <ScrollView style={{ flex: 1, padding: 10, maxHeight: 300 }}>

                <Text style={{ color: '#3d3d3d' }}>{this.state.detailsData}</Text>
              </ScrollView>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.openDetailDescription}
          onRequestClose={() => { this.setState({ openDetailDescription: false }) }}>
          <View style={styles.containerStyle}>
            <View style={styles.CardSectionStyle}>
              <View style={{ backgroundColor: '#000000', height: 70, flexDirection: 'row', paddingTop: 20, width: '100%' }}>
                <TouchableOpacity style={{ paddingLeft: 30 }} onPress={() => { this.setState({ openDetailDescription: false }) }}>
                  <Icon size={25} name='close' style={{ marginTop: 5, marginBottom: 10, marginRight: 5, color: '#fff' }} />
                </TouchableOpacity>
                <Text style={{ color: '#fff', fontSize: 20, marginTop: 5, fontFamily: 'Merriweather-Italic' }}>Free Activity</Text>
              </View>
              <View style={{padding:10,height:280}}>
                <Text style={{ fontSize: 16, fontWeight: '800', marginBottom: 10 }}>Description</Text>
                <ScrollView style={{ flex: 1, padding: 10, maxHeight: 300,height:250 }}>
                  <Text style={{ color: '#3d3d3d' }}>{this.state.detailDescription}</Text>
                </ScrollView>
              </View>
            </View>
          </View>
        </Modal>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor: '#fff'
  },
  scrollViewContent: {
    marginTop: 150,
    elevation: 0
  },
  CardSectionStyle: {
    backgroundColor: '#fff',
    position: 'relative',
    flexDirection: 'column',
    padding:10,
    paddingBottom: 20,
    height:300

  },
  containerStyle: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    position: 'relative',
    flex: 1,
    justifyContent: 'flex-end',
  }
});

const mapStateToProps = state => {
  return { itnery: state.itenarary };
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Itenrary_Timeline)
