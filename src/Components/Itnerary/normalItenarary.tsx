import React from 'react';
import { View } from 'react-native';

export default class NormalItnerary extends React.Component {
    render() {
        const { container, button } = styles;
        return (
            <View style={container}>
                <View style={button}>
                </View>
            </View>
        )
    }
}
const styles = {
    container: {
        width: 18,
        height: 18,
        borderRadius: 9,
        backgroundColor: '#000',
        position: 'absolute',
        top: 30,
        left: 10,
        padding: 4,
        elevation:2,
        zIndex:9
    },
    button: {
        height: 10,
        width: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
    }
}