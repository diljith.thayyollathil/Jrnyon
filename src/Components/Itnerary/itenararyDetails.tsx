import React from 'react';
import {View,Text, TouchableOpacity, ScrollView,Image} from 'react-native';
import {MediumButton} from '../common';
import Icon from 'react-native-vector-icons/MaterialIcons';

export interface activityList {
    id:number;
    name:string;
    desc:string;
    is_new:boolean;
  }
export interface activityDetailsProps {
    activityDetails:activityList[];
}
export interface activityDetailsState{

}
export default class ItneraryDetails extends React.Component<activityDetailsProps,activityDetailsState>{
    constructor(props:activityDetailsProps){
        super(props);
        this.state={

        }
    }
    render(){
        const {detailsTitle, detailsDescription, 
             equalipadding, newLabel, newLabelText, newActivity,heading,suggestionContainer} = styles;
        
        return(
            <View style={{flex:1,marginTop:10,marginBottom:10}}>
                    <View style={{marginBottom:20,marginTop:20,flexDirection:'row'}}>
                         <Image source={require('../resourses/img/activity.png')} style={{marginTop:-10,marginLeft:20,marginRight:10,height:50,width:50}} />
                        <Text style={[heading,{marginTop:3}]}>Additional Activity</Text>
                        <View style={{alignSelf:'flex-end',height:40,width:120,backgroundColor:'#FFAA58',
                        alignItems:'center',justifyContent:'center',position:'absolute',right:-20,top:-10,borderTopLeftRadius:5,borderBottomLeftRadius:5}}>
                        <Text style={{color:'#fff',width:110,fontFamily:'Merriweather-Bold',fontSize:12}}>NEW EVENTS</Text>
                        </View>
                    </View>
                    <ScrollView horizontal={true} style={{padding:10}} showsHorizontalScrollIndicator={false} >
                    {this.props.newEvent.map((item:any,i:number)=>{
                        return(
                            <View style={[suggestionContainer]} elevation={5} key={i}>
                                <View style={{flex:1,width:165,alignSelf:'flex-start',alignItems:'flex-start',paddingLeft:15}}>
                                    <View style={{alignItems:'flex-start',width:160,backgroundColor:'#ededed',borderRadius:14,height:32,marginTop:8,justifyContent:'center',borderColor:'#eaeaea',marginLeft:-5}}>
                                        <Text style={{textAlign:'center',fontSize:12,fontWeight:'700',width:140}}>{item.category}</Text> 
                                    </View>
                                    <View style={{alignItems:'flex-start',width:160,borderRadius:14,height:17,marginTop:5,paddingTop:3}}>
                                        <Text style={{textAlign:'left',width:140,fontSize:12,fontWeight:'800',color:'#767676',fontFamily:'Merriweather-Regular'}}>{item.name}</Text> 
                                    </View>
                                    <View style={{alignItems:'flex-start',width:160,borderRadius:14,height:15,marginTop:12}}>
                                        <Text style={{textAlign:'left',width:140,fontSize:12,color:'#767676',fontFamily:'Merriweather-Regular'}}>{item.location}</Text> 
                                    </View>
                                    <View style={{alignItems:'flex-start',width:160,borderRadius:14,height:15,marginTop:12}}>
                                        <Text style={{textAlign:'left',width:140,color:'#000',fontSize:12,fontFamily:'Merriweather-Regular'}}>{item.slot}</Text> 
                                    </View>
                                    <View style={{alignItems:'flex-start',width:160,height:15,marginTop:10,flexDirection:"row",justifyContent:'space-between'}}>
                                        <View style={{backgroundColor:'#00B1F1',height:40,width:80,borderRadius:5,alignItems:'center',justifyContent:'center'}}>
                                        <Text style={{color:'#fff',fontSize:15,fontWeight:'800',fontFamily:'Merriweather-Regular'}}>BOOK</Text></View>
                                        <TouchableOpacity onPress={this.props.open_Details_AdditionalActivity.bind(this,item.details)}><Text style={{color:'#FFAA58',fontSize:12,height:20,marginTop:10}}>View Details</Text></TouchableOpacity>
                                    </View>
                                </View>
                                <Image style={{width:100,height:180,alignSelf:'flex-end',borderRadius:8}} source={{uri:this.props.ip+'/media/'+item.image}} />
                                <View style={{height:30,width:80,backgroundColor:'#fff',
                                position:'absolute',right:0,top:20,justifyContent:'center',
                                alignItems:'center',shadowColor: '#767676',
                                shadowOffset: {
                                width: 0,
                                height: 3
                                },
                                shadowRadius: 5,
                                shadowOpacity: 1.0,flexDirection:'row',borderTopLeftRadius:5,borderBottomLeftRadius:5}}> 

                                    <Text style={{fontFamily:'Merriweather-Italic'}}>Rs. {item.price}</Text>
                                </View>
                            </View>
                        )
                    })}
                    </ScrollView>
            </View>
        )
    }
}

const styles = {
    heading:{
        fontWeight:'800',
        fontSize:15
    },
    detailsTitle:{
        fontWeight: 'bold',
        fontSize: 14,
        marginBottom:10,
        marginTop:10
    },
    suggestionContainer:{
        backgroundColor:'#fff',
        shadowColor: '#ccc',
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        height:180,
        width:295,
        borderRadius:8,
        marginLeft:20,
        marginTop:10,
        marginBottom:10
    },
    detailsDescription:{
        fontSize:12,
        lineHeight:19
    },
    detailsButton:{
        color:'#FF5B27',
        fontSize:16,
        marginTop:10,
        marginBottom:10,
        fontWeight:'bold'
    },
    newActivity:{
        marginLeft:20,
        backgroundColor:'#fff',
        borderColor:'#efefef',
        borderWidth:1,
        shadowColor: '#767676',
        shadowOffset: {
        width: 0,
        height: 0,
        },
    shadowRadius: 3,
    shadowOpacity: 0.1
    },
    equalipadding:{
        paddingLeft:20,
        paddingRight:20
    },
    newLabel:{
        backgroundColor:'#FFEFC0',
        height:25,
        borderRadius:3,
        width:100,
        alignItems:'center',
        paddingTop:4,
        marginTop:8,
        marginRight:-23
    },
    newLabelText:{
        fontWeight:'bold',
        color:'#FF5B27',
        fontSize:12
    },
    oldActivity:{

    }
}