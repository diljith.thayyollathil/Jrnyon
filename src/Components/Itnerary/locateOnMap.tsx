import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import openMap from 'react-native-open-maps';
import Icon from 'react-native-vector-icons/MaterialIcons';

class LocateMap extends React.Component {
    goToMap(){
        openMap({ latitude: this.props.locateMap.latitude, longitude:this.props.locateMap.longitude });
    }
    render() {
        let { buttonContainer, LocateText } = styles;
        return (
            <TouchableOpacity onPress={this.goToMap.bind(this)}>
                <View style={buttonContainer}>
                    <Icon style={{color:'#ffab5c'}} name='map' size={25} />
                    <Text style={LocateText}>LOCATION</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = {
    buttonContainer: {
        borderWidth: 1,
        borderColor: '#FFAB5C',
        alignItems: 'center',
        flexDirection: 'row',
        width: 125,
        height: 33,
        justifyContent:'center',
        borderRadius: 10,
        marginTop: 10,
        paddingLeft:13,
        marginBottom: 10,
    },
    LocateText: {
        fontSize: 12,
        fontWeight:'bold',
        color:'#FFAB5C',
        width:80
    }
}

export default LocateMap;