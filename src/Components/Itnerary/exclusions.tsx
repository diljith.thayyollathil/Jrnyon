import React from 'react';
import { View, ScrollView } from 'react-native';
import ExclusionIndividual from './exclusionsIndividual';

class Exclusions extends React.Component {
    render() {
        return (
            <View style={{ height: 100}}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                    {this.props.exclusions.map((item,i)=>{
                        return(
                            <ExclusionIndividual exclusion={item} key={i} />
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}
export default Exclusions;
