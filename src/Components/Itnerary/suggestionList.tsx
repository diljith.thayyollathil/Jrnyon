import React from 'react';
import { View, FlatList, Text, ScrollView } from 'react-native';
import SuggestionIndividual from './SuggestionIndividual';

class SuggestionList extends React.Component {
    render() {
        let suggestionlists = ['Places To Eat','Places To See'];
        return (
            <View style={{ height: 150, paddingLeft:10 }}>
                <ScrollView horizontal={true} style={{padding:10}} showsHorizontalScrollIndicator={false} >
                    {suggestionlists.map((item,i)=>{
                        return(
                            <SuggestionIndividual data={item} key={i}/>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}
export default SuggestionList;
