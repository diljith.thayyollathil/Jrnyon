import React from 'react';
import {Text,StyleSheet,Animated, TouchableOpacity} from 'react-native';
import { Icon } from 'react-native-material-ui';

class FloatingButton extends React.Component{
    render(){
        const {buttonContainer,AddIcon} = styles;
        return(
            
            <Animated.View style={[buttonContainer,{top:this.props.topValue}]}>
                <TouchableOpacity style={{height:40,width:40, alignItems:'center',
                justifyContent:'center'}} onPress={this.props.onButtonPressed}>
                    <Icon  size={25} name="add"></Icon>
                </TouchableOpacity>
            </Animated.View>
            
        )
    }
}
const styles=StyleSheet.create({
    buttonContainer:{
        height:50,
        width:50,
        backgroundColor:'#FFAA5D',
        position:'absolute',
        right:20,
        zIndex:9,
        borderRadius:28,
        alignItems:'center',
        justifyContent:'center'
    },
    AddIcon:{
        fontSize:25,
    }
})
export {FloatingButton};