import React from 'react';
import {View, Text} from 'react-native';

class SubHead extends React.Component{
    render(){
        return(
            <View style={{flex:1,paddingLeft:20}}>
                <Text style={{fontWeight:'bold',color:'#3d3d3d',fontSize:16}}>{this.props.children}</Text>
            </View>
        )
    }
}
export  {SubHead};