import React from 'react';
import { Animated, StyleSheet, View, BackHandler, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
class CustomHeaderBook extends React.Component {
    constructor(props) {
        super(props)
        this.backbuttonPress = this.backbuttonPress.bind(this);
    }
    
    backbuttonPress() {
        Actions.pop()
    }

    render() {
        const { backgroundHeader, bar, header, backButton, HeaderTitleStyle } = styles;
        return (
                
            <Animated.View style={[header, { height: this.props.headerHeight }]}>
                <Animated.View style={backgroundHeader} />
                <Animated.View style={bar}>
                    <TouchableOpacity onPress={this.backbuttonPress.bind(this)} style={{ position: 'absolute', right: 20,top:0 }}>
                        <Animated.Image source={require('../resourses/img/closeIcon.png')} style={[backButton,{height:this.props.backSize,width:this.props.backSize,marginTop:this.props.backPosition}]} />
                    </TouchableOpacity>
                    <Animated.Text style={[HeaderTitleStyle,{width:'65%',textAlign:'center',fontSize:this.props.TitleSize,marginTop:this.props.Titleposition,paddingTop:this.props.TitleHeight}]}>{this.props.HeaderTitle}</Animated.Text>
                </Animated.View>
            </Animated.View>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#0c0b08',
        overflow: 'hidden',
        zIndex:0
    },
    HeaderTitleStyle:{
        color:'#fff',
        alignSelf:'center',
        zIndex:0,
        height:50,
        fontFamily:'Merriweather-Italic'
    },
    bar: {
        height: 182,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundHeader: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: 'auto',
        height: 150,
        backgroundColor: '#0c0b08'
    },
    backButton: {
        height: 35,
        width: 35,
        zIndex:1
    }
})
export { CustomHeaderBook };