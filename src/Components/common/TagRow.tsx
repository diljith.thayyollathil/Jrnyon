import React from 'react';
import {ScrollView, View, Text, StyleSheet,Animated} from 'react-native';



class TagRow extends React.Component{
    render(){
        const {tagscontainer, container, containercontent} = styles;
        return(
            <Animated.ScrollView   style={[container,{marginTop:this.props.floatingPosition}]} contentContainerStyle={containercontent} horizontal={true} centerContent={true}>
                {this.props.tags.map((item,i)=>{
                    return(
                        <Animated.View style={[tagscontainer,{height:this.props.TagCapsuleHeight,padding:this.props.TagCapsulePadding}]} key={i}>
                            <Animated.Text style={[{minWidth:50},{fontSize:this.props.TagCapsuleFOntSIze,textAlign:'center',color:'#333',fontWeight:'600'}]}>{item.toUpperCase()}</Animated.Text>
                        </Animated.View>
                    )
                })}
            </Animated.ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        position:'absolute',
        left:0,right:0,
        zIndex:1,
    },
    containercontent:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    tagscontainer:{
        backgroundColor:'#FFEAD5',
        borderRadius:15,
        marginRight:10,
        paddingLeft:10,
        paddingRight:10,
    }
})
export {TagRow};