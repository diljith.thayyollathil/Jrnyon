import React from 'react';
import {
    Animated,
    TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

export interface headerProps {
    headerHeight: number;
    HeaderContent: Date;
    HeaderTitle:string;
    headertitlesOpacity:number;
    fontSizeDay:number;
    headerTitleMargin:number;
}
export interface headerState {
    Months: string[];
    WeekDay: string[];
}
export default class Header extends React.Component<headerProps, headerState>{
    constructor(props: headerProps) {
        super(props);
        this.state = {
            Months: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            WeekDay: ["Monday", "TuesDay", "Wednesday", "Thursday", "Friday", "Saturday"]
        }
    }
    backbuttonPress(){
        Actions.pop();
    }
    render() {
        const { header, backgroundHeader, bar, title, backButton } = styles;
        const headerdate = this.props.HeaderContent;
        return (
            <Animated.View style={[header, { height: this.props.headerHeight }]}>
                <Animated.View style={backgroundHeader} />
                <Animated.View style={bar}>
                     <TouchableOpacity onPress={this.backbuttonPress.bind(this)} style={{ position: 'absolute', left: 20 }}>
                        <Animated.Image source={require('../resourses/img/back.png')} style={[backButton,{height:this.props.backSize,width:this.props.backSize,marginTop:this.props.backPosition}]} />
                    </TouchableOpacity>
                    <Animated.Text style={[title, { opacity:this.props.headertitlesOpacity,position:'absolute',top:1,fontSize:16,fontWeight:'bold'}]}>{this.props.HeaderTitle}</Animated.Text>
                    <Animated.Text style={[title, { fontSize: this.props.fontSizeDay ,marginTop:this.props.headerTitleMargin}]}>{this.props.duration}</Animated.Text>
                </Animated.View>
            </Animated.View>
        );
    }
};

const styles = {
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#0c0b08',
        overflow: 'hidden',
    },
    backButton: {
        height: 35,
        width: 35,
        zIndex:1
    },
    bar: {
        marginTop: 28,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        backgroundColor: 'transparent',
        color: 'white',
    },
    backgroundHeader: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: 'auto',
        height: 150,
        backgroundColor: '#0c0b08'
    }
};
