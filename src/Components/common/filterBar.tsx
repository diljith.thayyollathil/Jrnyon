import React from 'react';
import {View, Image,Text,StyleSheet,TouchableOpacity} from 'react-native';

class FilterRow extends React.Component{
    render(){
        const {filterContainer, filterIcon, numberOfInvitees, inviteesInfo
        ,inviteesInfoContainer,filterIconContainer } = styles;
        return(
            <View style={filterContainer}>
                <TouchableOpacity style={filterIconContainer} onPress={this.props.showFIlter}>
                    <Image style={filterIcon}  source={require('../resourses/img/filter.png')} />
                </TouchableOpacity>
                <Text style={inviteesInfoContainer}>
                    <Text style={numberOfInvitees}>{this.props.Count} </Text>
                    <Text style={inviteesInfo}> Invitees in this trip</Text>
                </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    filterContainer:{
        height:40,
        backgroundColor:'#f0f0f0',
        alignItems:'center',
        marginTop:150,
        zIndex:1
    },
    filterIconContainer:{
        left:10,
        position:'absolute',
    },
    filterIcon:{
        height:40,
        width:40,
        opacity:0.5
    },
    numberOfInvitees:{
        fontWeight:'600'
    },
    inviteesInfo:{
        color:'#767676'
    },
    inviteesInfoContainer:{
        marginTop:12
    }
});
export {FilterRow}