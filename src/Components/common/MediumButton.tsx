import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const MediumButton = ({ onPress, children }) => {
    const { buttonStyles, textStyle } = styles;
    return (
        <TouchableOpacity onPress={onPress} style={buttonStyles}>
            <Text style={textStyle}>{children} </Text>
        </TouchableOpacity>
    );
};
const styles = {
    buttonStyles: {
        width:90,
        alignSelf: 'stretch',
        backgroundColor: '#FF9835',
        margin: 10,
        marginLeft:0,
        borderRadius:7
    },
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10,

    }
}

export { MediumButton };
