import React from 'react';
import { ScrollView, View, Animated, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { CustomHeader } from '../common';
import TripIndividual from '../home/tripindividual';

class AllUpcoming extends React.Component{
    constructor(props: inviteesProps) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0),
        }
    }
    componentDidMount(){
        this.props.actions.fetch_all_upcoming_trips()
    }
    render(){
        const HEADER_SCROLL_DISTANCE = 150 - 80;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [150, 80],
            extrapolate: 'clamp',
        });

        const floatingPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [125, 55],
            extrapolate: 'clamp',
        });

        const backSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 25],
            extrapolate: 'clamp',
        });

        const backPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [65, -10],
            extrapolate: 'clamp',
        });
        const Titleposition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [30, 0],
            extrapolate: 'clamp',
        });
        const TitleSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [25, 20],
            extrapolate: 'clamp',
        });
        const TitleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [50, 30],
            extrapolate: 'clamp',
        });
        return(
            <View style={styles.fill}>
                <ScrollView
                    style={styles.fill}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
                    }>
                    <View style={{marginTop:140}}>
                        {(this.props.upComing.trips!=undefined && this.props.upComing.trips.length!=0) && this.props.upComing.trips.map((item,i)=>{
                            return(
                                <TripIndividual type='allupcoming' startDate={item.start_date_in_format1}  invited={true} trip={item} key={i} />
                            )
                        })}
                    </View>
                </ScrollView>
                <CustomHeader headerHeight={headerHeight}
                    backPosition={backPosition}
                    backSize={backSize}
                    HeaderTitle="Upcoming Trips"
                    Titleposition={Titleposition}
                    TitleSize={TitleSize}
                    TitleHeight={TitleHeight}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fff'
    }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(AllUpcoming);
