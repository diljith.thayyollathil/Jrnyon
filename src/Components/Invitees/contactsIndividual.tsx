import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

class ContactsIndividual extends React.Component {
    render() {
        console.log
        const { inviteRow, userDP } = styles;
        return (
            <View style={inviteRow}>
            
                <View style={{ width: '20%' }}>
                    <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMjO9lsBeDLzlm9780HyMXWa0c07DnpzPy_CCYoc3kqX85WGh4Mw' }} style={userDP} />
                </View>
                <View style={{ width: '50%', paddingTop: 7 }}>
                    <Text style={{ marginBottom: 4, fontWeight: '600' }}>{this.props.contact.givenName}</Text>
                    <Text style={{ color: '#757575', fontWeight: '400' }}>{this.props.contact.phoneNumbers[0]!=undefined ? this.props.contact.phoneNumbers[0].number:'-'}</Text>
                </View>
                <View style={{width:'30%',paddingTop:7, alignContent:'flex-end',flex:1}}>
                {this.props.contact.selected==true &&<Image style={{height:25,width:25,alignSelf:'flex-end'}} source={{uri:'https://www.jowgar.com.au/wp-content/uploads/yellow-orange-tick.png'}}/> }
                    
                </View>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    inviteRow: {
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3',
        flexDirection: 'row',
        padding: 10,
        backgroundColor: '#fff',
        flex:1
    },
    userDP: {
        height: 50,
        width: 50,
        borderRadius: 25
    }
})
export default ContactsIndividual;