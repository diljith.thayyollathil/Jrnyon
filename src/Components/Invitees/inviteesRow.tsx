import React from 'react';
import {View,Text,Image, StyleSheet} from 'react-native';

class InviteeIndividual extends React.Component{
    
    render(){
        const {inviteRow, userDP, paidIcon, paidLabel, inviteRowAvoided} = styles;
        return(
            <View style={this.props.inviteelength==(this.props.itration+1)? inviteRowAvoided:inviteRow}>
                <View style={{width:'20%'}}>
                    <Image source={require('../resourses/img/profile.png')} style={userDP}/>
                </View>
                <View style={{width:'50%',paddingTop:7}}>
                    <Text style={{marginBottom:4,fontWeight:'600'}}>{this.props.invite.name}</Text>
                    <Text style={{color:'#757575',fontWeight:'400'}}>{this.props.invite.mobile_no!=undefined?this.props.invite.mobile_no:this.props.invite.phone}</Text>
                </View>
                <View style={{width:'30%',justifyContent: 'center',alignItems: 'flex-end'}}>
                    {this.props.invite.status==='paid' && <View style={{flex:1}}>
                        <Image style={paidIcon} source={require('../resourses/img/tick_green.png')}/>
                        <Text style={paidLabel}>Paid</Text>
                    </View>}
                    {(this.props.invite.status==='Rejected'||this.props.invite.status==='Accepted')&&
                     <Text style={{fontSize:12}}>{this.props.invite.status}</Text>}
                     {this.props.invite.status==='Not Responded'&&
                     <Text style={{opacity:0.5,fontSize:12}}>{this.props.invite.status}</Text>}
                    
                </View>
                
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    inviteRow:{
        borderBottomWidth:1,
        borderBottomColor:'#e3e3e3',
        flexDirection:'row',
        padding:10,
        backgroundColor:'#fff'
    },
    inviteRowAvoided:{
        borderBottomWidth:1,
        borderBottomColor:'#fff',
        flexDirection:'row',
        padding:10,
        backgroundColor:'#fff'
    },
    userDP:{
        height:50,
        width:50,
        borderRadius:25
    },
    paidIcon:{
        height:20,
        width:20,
        position:'absolute',
        top:12
    },
    paidLabel:{
        color:'#82b340',
        fontWeight:'800',
        marginLeft:23,
        marginTop:14,
        fontSize:12
    }
})
export default InviteeIndividual;