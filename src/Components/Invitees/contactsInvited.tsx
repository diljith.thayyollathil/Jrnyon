import React from 'react';
import {View,Image,Text,Dimensions} from 'react-native';
class ContactInvited extends React.Component{
    render(){
        const window = Dimensions.get('window');
        const itemWidth = window.width/5;
        return(
            <View style={{width:itemWidth,height:45,alignItems:'center',padding:5,paddingBottom:0}}>
                <Image style={{height:25,width:25,borderRadius:13}} source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMjO9lsBeDLzlm9780HyMXWa0c07DnpzPy_CCYoc3kqX85WGh4Mw'}} />
                <Text style={{fontSize:12}}>
                    {this.props.contact.givenName}
                </Text>
            </View>
        )
    }
}
export default ContactInvited;