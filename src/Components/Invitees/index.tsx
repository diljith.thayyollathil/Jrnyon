import React from 'react';
import { ScrollView, View, Animated, StyleSheet, Text,
     TouchableOpacity , Modal, Image, Dimensions, PermissionsAndroid,Platform} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { CustomHeader, FilterRow, FloatingButton } from '../common';
import InviteeIndividual from './inviteesRow';
import ContactsWrapper from 'react-native-contacts-wrapper';
import { Icon } from 'react-native-material-ui';
import Contacts from "react-native-contacts";
import ContactsIndividual from './contactsIndividual';
import ContactInvited from './contactsInvited';

export interface inviteesProps {

}
export interface inviteesClass {

}
class Invitees extends React.Component<inviteesProps, inviteesClass>{
    constructor(props: inviteesProps) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0),
            showFilter: false,
            showContactsList:false,
            contacts:[],
            addedMembers:[],
            showConfirmPopup:false,
            openMoreContactDetails:false
        }
        this.onButtonPressed = this.onButtonPressed.bind(this);
        this.confirmPopUp = this.confirmPopUp.bind(this);
    }
    showFIlters() {
        this.setState({ showFilter: true })
    }
    filterContentWithtag() {
        this.setState({ showFilter: false })
    }
    confirmPopUp(){
        if(this.state.addedMembers.length!=0){
            this.setState({showConfirmPopup:true});
        }
    }
    onButtonPressed() {
        if (Platform.OS === 'ios') {
            Contacts.getAll((err, contacts) => {
                if (err) throw err;
                const newList: any = [];
                contacts.map((item: any, k: any) => {
                    item['id'] = k;
                    item['selected'] = false;
                    newList.push(item);
                })
                this.setState({ contacts: newList });
                this.setState({ showContactsList: true });
            })
        } else {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Contacts',
                    'message': 'This app would like to view your contacts.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err) throw err;
                    const newList: any = [];
                    
                    contacts.map((item: any, k: any) => {
                        item['id'] = k;
                        item['selected'] = false;
                        newList.push(item);
                    })
                    this.setState({ contacts: newList });
                   
                    this.setState({ showContactsList: true });
                })
            })
            
        }
        // ContactsWrapper.getContact()
        //     .then((contact) => {
        //         let newData = contact;
        //         newData['phone'] = newData.phone.replace(/ /g,'');
        //         console.log(newData)
        //         newData['id'] = this.props.id;
        //         this.props.actions.addInviteesToList(newData);
        //         console.log(newData,"longs");
        //     })
        //     .catch((error) => {
        //         console.log("Error occured",error)
        //     });
    }
    componentDidMount() {
        this.props.actions.fetch_invitees(this.props.id);
    }
    sendContactsInvites(){
        let newData = [];
        let dataToSend={};
        this.state.addedMembers.map((data,l)=>{
            let phono = '';
            data.phoneNumbers.map((item,k)=>{
                if(item.label = 'mobile'){
                    phono = item.number.replace(/[^\w\s]/gi, '');
                }
            })
            newData.push({
                phone:phono.replace(/ /g,''),
                name:data.givenName
            });
            dataToSend = {
                id:this.props.id,
                inviteeData:newData
            };
            
        })
        this.props.actions.addInviteesToList(dataToSend);
        
        let contacts = this.state.contacts;
        contacts.map((item,i)=>{
            item.selected = false;
        })
        this.setState({showConfirmPopup:false,addedMembers:[],contacts:contacts,openMoreContactDetails:false});
    }
    cancelSelectedContacts(){
        let contacts = this.state.contacts;
        contacts.map((item,i)=>{
            item.selected = false;
        })
        this.setState({showConfirmPopup:false,addedMembers:[],contacts:contacts,openMoreContactDetails:false});
    }
    selcted_or_deselect(item:any){
        let contacts = this.state.contacts;
        contacts.map((contacts,i)=>{
            if(contacts.id==item.id){
                if(contacts.selected==true){
                    contacts.selected = false;
                }else{
                    contacts.selected = true;
                }
            }
        })
        this.setState({contacts:contacts});
        const selected = contacts.filter((item:any)=>{
            return item.selected == true;
        });
        this.setState({addedMembers:selected});

    }
    render() {
        const HEADER_SCROLL_DISTANCE = 150 - 80;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [150, 80],
            extrapolate: 'clamp',
        });

        const floatingPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [125, 55],
            extrapolate: 'clamp',
        });

        const backSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 25],
            extrapolate: 'clamp',
        });

        const backPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [65, -10],
            extrapolate: 'clamp',
        });
        const Titleposition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [30, 0],
            extrapolate: 'clamp',
        });
        const TitleSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [25, 20],
            extrapolate: 'clamp',
        });
        const TitleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [50, 30],
            extrapolate: 'clamp',
        });
        const inviteelength = this.props.invitees.length;
        const window = Dimensions.get('window');
        const itemWidth = window.width/5;
        return (
            <View style={styles.fill}>
                <FloatingButton topValue={floatingPosition} onButtonPressed={this.onButtonPressed} />
                <ScrollView
                    style={styles.fill}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
                    }>
                    {this.props.invitees!=undefined && <FilterRow Count={this.props.invitees.length} showFIlter={this.showFIlters.bind(this)} />}
                    
                    {this.state.showFilter == true && <View style={{ position: 'absolute', left: 10, top: 180, width: 150, height: 230,paddingTop:10,paddingLeft:15,borderRadius:5, backgroundColor: '#fff', borderWidth: 1, borderColor: '#ccc', zIndex: 999 }}>
                        <Text style={{ fontWeight: '800', fontSize: 13, textAlign: 'left', padding: 10 }}>Filter By</Text>
                        <TouchableOpacity onPress={this.filterContentWithtag.bind(this)}>
                            <Text style={{ fontSize: 12, color: '#767676', padding: 10, textAlign: 'left' }}>Paid</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.filterContentWithtag.bind(this)}>
                            <Text style={{ fontSize: 12, color: '#767676', padding: 10, textAlign: 'left' }}>Accepted</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.filterContentWithtag.bind(this)}>
                            <Text style={{ fontSize: 12, color: '#767676', padding: 10, textAlign: 'left' }}>Rejected</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.filterContentWithtag.bind(this)}>
                            <Text style={{ fontSize: 12, color: '#767676', padding: 10, textAlign: 'left' }}>Not Responded</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.filterContentWithtag.bind(this)}>
                            <Text style={{ fontSize: 12, color: '#767676', padding: 10, textAlign: 'left' }}>All</Text>
                        </TouchableOpacity>
                    </View>}
                    <View style={{ paddingLeft: 15, paddingRight: 15, backgroundColor: '#fff' }}>
                        {this.props.invitees != undefined && this.props.invitees.map((item, i) => {
                            return (
                                <InviteeIndividual inviteelength={inviteelength} itration={i} key={i} invite={item} />
                            )
                        })}
                    </View>
                </ScrollView>
                <CustomHeader headerHeight={headerHeight}
                    backPosition={backPosition}
                    backSize={backSize}
                    HeaderTitle="Invitees"
                    Titleposition={Titleposition}
                    TitleSize={TitleSize}
                    TitleHeight={TitleHeight}
                />
                <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.showContactsList}
                        onRequestClose={()=> { this.setState({showPopup:false}) }}>
                        <View style={styles.containerStyle}>
                            <View style={styles.CardSectionStyle}>
                                <View style={{backgroundColor:'#000000',height:70,flexDirection:'row',paddingTop:20,width:'100%'}}>
                                    <TouchableOpacity style={{paddingLeft:30}} onPress={()=>{this.setState({showContactsList:false})}}>
                                        <Icon size={25} name='close' style={{ marginTop: 5,marginBottom:10, marginRight: 5,color:'#fff' }} />
                                    </TouchableOpacity>
                                    <Text style={{color:'#fff',fontSize:20,marginTop:5,fontFamily:'Merriweather-Italic'}}>Invite Contacts</Text>
                                </View>
                               <ScrollView style={{flex:1}}>
                               
                                {this.state.contacts.map((item:any, i:number)=> {
                                    return(
                                        <TouchableOpacity key={i} onPress={this.selcted_or_deselect.bind(this,item)} style={{flex:1}}>
                                            <ContactsIndividual contact={item} />
                                        </TouchableOpacity>
                                    )
                                })}
                              
                               </ScrollView>
                               <View style={{flex:1,position:'absolute',bottom:0,left:0,right:0,height:60,backgroundColor:'#FFAA5D',justifyContent:'center',alignItems:'center'}}>
                                    <TouchableOpacity onPress={this.confirmPopUp.bind(this)}>
                                    <Text>Invite {this.state.addedMembers.length==0?'':this.state.addedMembers.length}</Text>
                                    </TouchableOpacity>
                               </View>
                               {this.state.showConfirmPopup==true && 
                                        <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, top: 0, backgroundColor: 'rgba(0,0,0,0.5)', flex: 1, zIndex: 9999 }}>
                                        <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, flex: 1, backgroundColor: '#fff', zIndex: 2 }}>
                                            <Text style={{ fontSize: 20, fontFamily: "Merriweather-italic", textAlign: 'center', padding: 20 }}>Invite Message</Text>
                                            <Text style={{ textAlign: 'center', color: '#767676', fontSize: 13, padding: 20, paddingLeft: 58, paddingRight: 58 }}>{this.props.nameOfInvitee} has invited you to join them on the trip " {this.props.nameOfTrip}" organized by JrnyOn.
                                             View more details here 'http://www.jrnyon.com'</Text>
                                             {this.state.openMoreContactDetails==false && <ScrollView horizontal={true} style={{flex:1,marginBottom:30}}>
                                            {(this.state.addedMembers.length<=4) && <View style={{flex:1,flexDirection:'row'}}>
                                                {this.state.addedMembers.map((item:any, p:any)=>{
                                                    return(
                                                        <ContactInvited key={p} contact={item} />
                                                    )
                                                })}
                                            </View>}
                                            {(this.state.addedMembers.length>4) && <View style={{flex:1,flexDirection:'row'}}>
                                                {this.state.addedMembers.map((item:any, p:any)=>{
                                                    return(<View key={p}>
                                                            {p<=3 && <ContactInvited contact={item} />}
                                                        </View>
                                                    )
                                                })}
                                                <TouchableOpacity onPress={()=> this.setState({openMoreContactDetails:true})}>
                                                <View style={{width:itemWidth,height:45,alignItems:'center',padding:5,paddingBottom:0}}>
                                                    <View style={{height:25,width:25,borderRadius:13,backgroundColor:"#efefef",alignItems:'center',justifyContent:'center',alignContent:'center'}}>
                                                     <Text>{this.state.addedMembers.length-4}</Text>
                                                    </View>
                                                    <Text style={{fontSize:12}}>
                                                         more
                                                    </Text>
                                                </View>
                                                </TouchableOpacity>
                                            </View>}

                                                
                                            </ScrollView>}

                                            {this.state.openMoreContactDetails==true &&<View> 
                                                    <TouchableOpacity style={{alignContent:'flex-end',alignItems:'flex-end'}} onPress={()=>this.setState({openMoreContactDetails:false})}>
                                                        <Text style={{fontWeight:'800',fontSize:12,marginRight:15,marginBottom:10}}>Show Less</Text>
                                                    </TouchableOpacity>
                                                <ScrollView style={{flex:1,width:'100%',maxHeight:200}}>
                                                <View style={{flex:1,flexDirection:'row',flexWrap:'wrap',alignItems: 'flex-start'}}>
                                                {this.state.addedMembers.map((item:any, p:any)=>{
                                                    return( <ContactInvited key={p} contact={item} />
                                                        
                                                    )
                                                })}
                                                </View>
                                            </ScrollView></View>}

                                            
                                            <View style={{flex:1,width:'100%',flexDirection:'row'}}>
                                            <TouchableOpacity style={{width:'50%', height: 50, borderTopWidth: 1, borderTopColor: '#ccc', paddingTop: 17,backgroundColor:'#ffaa5d' }} onPress={this.sendContactsInvites.bind(this)}>
                                                <Text style={{ textAlign: 'center', color: '#767676' }}>Send Invites</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width:'50%',height: 50, borderTopWidth: 1, borderTopColor: '#ccc', paddingTop: 17 }} onPress={this.cancelSelectedContacts.bind(this)}>
                                                <Text style={{ textAlign: 'center', color: '#767676' }}>Cancel</Text>
                                            </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                }
                                
                            </View>
                        </View>
                        </Modal>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fff'
    },
    CardSectionStyle: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        paddingBottom: 20,
        flex:1,
    },
    containerStyle: {
        backgroundColor: '#fff',
        position: 'relative',
        flex: 1,
    }
});
const mapStateToProps = state => {
    return state.invitees;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Invitees);