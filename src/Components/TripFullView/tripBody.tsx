import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';

class TripBody extends React.Component {
    movetoItenerary(id, dur, e) {
        let ids = id;
        let duration = dur;
        Actions.itnerary_timeline({ id: ids, duration: duration, ip: this.props.ip })
    }
    render() {
        const { container, tripRow, rupeeSymbol, tripCost, responce,
            Invitee } = style;
        let packageId = this.props.trip != undefined ? this.props.trip.id : '-'
        let duration = this.props.trip != undefined ? (this.props.trip.duration_days + " D , " + this.props.trip.duration_nights + " N") : '-'
        let description = this.props.trip!=undefined ? this.props.trip.short_description: '-';
        return (
            <View style={container}>
                <View style={tripRow}>
                    <View style={{ width: '50%', flexDirection: 'row' }}>

                        <Image style={rupeeSymbol} source={require('../resourses/img/rupee.jpg')} />
                        <Text style={tripCost}>{this.props.trip != undefined ? this.props.trip.amount : '-'}</Text><Text style={{ color: '#767676', fontSize: 12 }}>/ Adult</Text>
                    </View>
                    <View style={{ width: '50%', justifyContent: 'flex-end', flexDirection: 'row' }}>
                            {this.props.trip!=undefined
                                && <StarRating
                                disabled={true}
                                maxStars={5}
                                rating={this.props.trip.rating}
                                starSize={15}
                                fullStarColor="#ffab5c"
                            />
                            }
                        <Text style={{ alignSelf: 'flex-end', textAlign: 'right' }}>({this.props.trip != undefined ? this.props.trip.rating : '-'})</Text>

                    </View>


                </View>
                <View style={[tripRow]}>
                    <Text style={{ color: "#767676", maxHeight: 70, overflow: 'hidden' }}>{description}</Text>
                    <TouchableOpacity onPress={this.props.openDescription.bind(this,description)} style={{ position: 'absolute', right: 0, bottom: 20 }}>
                        <Text style={{ fontWeight: '800', backgroundColor: '#fff', width: 80, paddingLeft: 5 }}>...More</Text>
                    </TouchableOpacity>
                </View>
                <View style={[tripRow, { backgroundColor: '#efefef', paddingTop: 0, paddingBottom: 0 }]}>
                    {/* <View style={{ width: '50%', flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#cccccc', paddingTop: 10, paddingBottom: 10 }}>
                        <Image style={Invitee} source={require('../resourses/img/calender.png')} />
                        <View style={{ padding: 5 }}>
                            <Text style={{ fontSize: 11, color: '#767676', marginBottom: 6 }}>Duration</Text>
                            <Text style={{ fontSize: 13, fontWeight: '800', fontFamily: 'Merriweather-Bold' }}>{this.props.trip != undefined ? (this.props.trip.duration_days + " D , " + this.props.trip.duration_nights + " N") : '-'}</Text>
                        </View>
                    </View> */}
                    <View style={{ width: '100%', flexDirection: 'row', paddingTop: 10, paddingBottom: 10, paddingLeft: 20,alignItems:'center',justifyContent:'center' }}>
                        <Image style={Invitee} source={require('../resourses/img/calendar.png')} />
                        <View style={{ padding: 5 }}>
                            <Text style={{ fontSize: 11, color: '#767676', marginBottom: 3 }}>Start Date</Text>
                            <Text style={{ fontSize: 13, fontWeight: '800', fontFamily: 'Merriweather-Bold' }}>{this.props.trip != undefined ? this.props.trip.start_date : '-'}</Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity onPress={this.movetoItenerary.bind(this, packageId, duration)} style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start' ,flexDirection:'row'}]}>
                    <Image style={{height:40,width:40,marginRight:10}} source={require('../resourses/img/itinerary.png')} />
                    <View style={{ width: '100%', alignItems: 'flex-start' }} ><Text style={{ fontSize: 13, textAlign: 'center' }}>Itinerary</Text></View>
                </TouchableOpacity>
                
                <View style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', marginBottom: 60,flexDirection:'row'}]}>
                <Image style={{height:40,width:40,marginRight:10}} source={require('../resourses/img/more_info.png')} />
                    <Text style={{ fontSize: 13, textAlign: 'center' }}>Detailed Information</Text>
                </View>
            </View>
        )
    }
}
const style = StyleSheet.create({
    container: {
        flex: 1
    },
    tripRow: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        padding: 20,
        flexDirection: 'row'
    },
    rupeeSymbol: {
        height: 15,
        width: 15,
        opacity: 0.7
    },
    tripCost: {
        fontWeight: '800',
        fontSize: 18,
        color: '#3d3d3d',
        alignSelf: 'flex-start',
        marginTop: -6,
        fontFamily: 'Merriweather-Bold'
    },
    responce: {
        fontSize: 12,
        color: '#767676'
    },
    Invitee: {
        height: 50,
        width: 50,
        borderRadius: 20
    }
})
export default TripBody;