import React from 'react';
import {View, Image, StyleSheet, Dimensions} from 'react-native';

class TripImage extends React.Component{
    render(){
        const {posterStyle} = styles;
        return(
            <View style={{flex:1,marginTop:150}}>
                <Image style={posterStyle} source={{uri:(this.props.ip+"/media/"+this.props.poster)}}/>
            </View>
        )
    }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
    posterStyle:{
        flex: 1,
        alignSelf: 'stretch',
        width: win.width,
        height: 200,
    }
})
export default TripImage;