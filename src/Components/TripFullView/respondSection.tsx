import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';

class RespondRow extends React.Component {
    render() {
        return (
            <View style={{
                flex: 1, position: 'absolute', bottom: 0, left: 0,
                right: 0, height: 60, zIndex: 1, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#ccc', shadowColor: '#ccc',
                shadowOffset: {
                    width: 0,
                    height: 0,
                },
                shadowRadius: 3,
                shadowOpacity: 0.3,
            }}>
                <View style={{ height: 60, width: '100%', padding: 20, backgroundColor: '#FFAA5D', alignItems: 'center' }}>
                    <TouchableOpacity disabled={(this.props.clickable || this.props.status === 'true') ? true : false} onPress={this.props.enquireToTeam.bind(this, this.props.packageId)}>
                        <Text>{(this.props.clickable === true || this.props.status === 'true') ? 'ENQUIRY SENT' : 'ENQUIRE'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(RespondRow)
