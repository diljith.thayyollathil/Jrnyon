import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { ScrollView, View, Animated, StyleSheet, Text, TouchableOpacity, Image , Modal} from 'react-native';

import { Icon } from 'react-native-material-ui';

import { CustomHeader1, TagRow } from '../common';
import TripImage from './tripRepresentImage';
import TripBody from './tripBody';
import RespondRow from './respondSection';

export interface tripProps {

}
export interface tripClass {
    enquireConfirmation: boolean;
    scrollY: any;
    clickable: boolean;
    openDescrionPopUp:boolean;
    descriptionData:string;
}
class TripView extends React.Component<tripProps, tripClass>{
    constructor(props: tripProps) {
        super(props);
        this.state = {
            enquireConfirmation: false,
            scrollY: new Animated.Value(0),
            clickable: false,
            openDescrionPopUp:false,
            descriptionData:''
        }
        this.onButtonPressed = this.onButtonPressed.bind(this);
        this.openDescription = this.openDescription.bind(this);
    }
    componentDidMount() {
        let id = this.props.id;
        this.props.actions.fetchTripDetails(id);
    }
    onButtonPressed() {
        console.log('sdsa');
    }
    enquireToTeam(packageId:any) {
        this.setState({ enquireConfirmation: true },
            () => {
                console.log(this.state);
                this.props.actions.enquire_to_team(packageId);
            });
    }
    hideEnquiry() {
        this.setState({ clickable: true });
        this.setState({ enquireConfirmation: false });
    }
    openDescription(data:string){
        this.setState({openDescrionPopUp:true})
        this.setState({descriptionData:data})
    }
    render() {
        const { fill } = styles;
        const HEADER_SCROLL_DISTANCE = 150 - 80;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [150, 80],
            extrapolate: 'clamp',
        });

        const floatingPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [131, 64],
            extrapolate: 'clamp',
        });

        const backSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 25],
            extrapolate: 'clamp',
        });

        const backPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [65, -10],
            extrapolate: 'clamp',
        });
        const Titleposition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [15, -30],
            extrapolate: 'clamp',
        });
        const TitleSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [20, 18],
            extrapolate: 'clamp',
        });
        const TitleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 30],
            extrapolate: 'clamp',
        });
        const TripcodeMargin = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [5, -18],
            extrapolate: 'clamp',
        });
        const TripcodeFontSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [14, 10],
            extrapolate: 'clamp',
        });
        const TagCapsuleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [32, 28],
            extrapolate: 'clamp',
        });
        const TagCapsulePadding = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [10, 8],
            extrapolate: 'clamp',
        });
        const TagCapsuleFOntSIze = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [12, 10],
            extrapolate: 'clamp',
        });

        return (
            <View style={styles.fill}>
                <TagRow tags={this.props.selected.trip != undefined ? this.props.selected.trip.tags : []}
                    floatingPosition={floatingPosition}
                    TagCapsuleHeight={TagCapsuleHeight}
                    TagCapsulePadding={TagCapsulePadding}
                    TagCapsuleFOntSIze={TagCapsuleFOntSIze}
                />
                {this.props.selected.trip!=undefined && <RespondRow status={this.props.selected.trip != undefined ? this.props.selected.trip.enquire : '-'} clickable={this.state.clickable} packageId={!!this.props.selected.trip != undefined ? this.props.selected.trip : ''} enquireToTeam={this.enquireToTeam.bind(this)} />}
                
                <ScrollView
                    style={styles.fill}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
                    }>
                    <TripImage ip={this.props.ipadress.ipAdress} poster={this.props.selected.trip != undefined ? this.props.selected.trip.images[0] : '-'} />
                    <TripBody openDescription={this.openDescription.bind(this)} ip={this.props.ipadress.ipAdress} trip={this.props.selected.trip} />
                </ScrollView>
                {this.state.enquireConfirmation === true && <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, top: 0, backgroundColor: 'rgba(0,0,0,0.5)', flex: 1, zIndex: 9999 }}>
                    <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, flex: 1, height: 254, backgroundColor: '#fff', zIndex: 2 }}>
                        <Text style={{ fontSize: 20, fontFamily: "Merriweather-italic", textAlign: 'center', padding: 20 }}>We Will Get In Touch</Text>
                        <View style={{ alignContent: 'center', alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            <Image style={{ width: 50, height: 50, marginRight: 10 }} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAb2GoiLTaOTuNxW9s4qz1afqhFc3swOwfw1kMenJ83zXqJBrW' }} />
                            <Image style={{ width: 50, height: 50, marginRight: 10 }} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfdikQHNtpxpufc3B7lHxRWNviqkQsXlprIbOy2N7ZHJhgVz5J' }} />
                            <Image style={{ width: 50, height: 50, marginRight: 10 }} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAb2GoiLTaOTuNxW9s4qz1afqhFc3swOwfw1kMenJ83zXqJBrW' }} />
                        </View>
                        <Text style={{ textAlign: 'center', color: '#767676', fontSize: 13, padding: 20, paddingLeft: 58, paddingRight: 58 }}>Our folks will be in touch with the details of this trip and will discuss any customization required</Text>
                        <TouchableOpacity style={{ height: 50, borderTopWidth: 1, borderTopColor: '#ccc', paddingTop: 17 }} onPress={this.hideEnquiry.bind(this)}>
                            <Text style={{ textAlign: 'center', color: '#767676' }}>Ok</Text>
                        </TouchableOpacity>
                    </View>
                </View>} 
                <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.openDescrionPopUp}
                onRequestClose={() => { this.setState({ openDescrionPopUp: false }) }}>
                <View style={styles.containerStyle}>
                    <View style={styles.CardSectionStyle}>
                    <TouchableOpacity onPress={() => { this.setState({ openDescrionPopUp: false }) }}>
                        <Icon size={20} name='close' style={{ alignSelf: 'flex-end', marginTop: 5, marginBottom: 10, marginRight: 5, color: '#000' }} />
                    </TouchableOpacity>
                    <Text style={{fontSize:16,fontWeight:'800',marginBottom:10}}>Descriptions</Text>
                    <ScrollView style={{flex:1,padding:10,maxHeight:300}}>
                        <Text style={{color:'#3d3d3d'}}>{this.state.descriptionData}</Text>
                    </ScrollView>
                    </View>
                </View>
                </Modal>


                <CustomHeader1 headerHeight={headerHeight}
                    backPosition={backPosition}
                    backSize={backSize}
                    HeaderTitle={this.props.selected.trip != undefined ? this.props.selected.trip.name : '-'}
                    Titleposition={Titleposition}
                    TitleSize={TitleSize}
                    TitleHeight={TitleHeight}
                    TripcodeMargin={TripcodeMargin}
                    TripcodeFontSize={TripcodeFontSize}
                    tripcode={this.props.selected.trip != undefined ? this.props.selected.trip.trip_code : '-'}
                /> 
            </View>
        )
    }
}
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fff'
    },
    CardSectionStyle: {
        padding: 10,
        backgroundColor: '#fff',
        position: 'relative',
        flexDirection: 'column',
        paddingBottom: 20,
        height:300
      },
      containerStyle: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        position: 'relative',
        flex: 1,
        justifyContent: 'flex-end',
      }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(TripView)