import * as React from 'react';
import { View, Animated, TouchableOpacity, StyleSheet } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import InvitesTab from './invites';
import CurrentTab from './current';

import UpcomingTab from './upcoming';


const Current = () => (
    <CurrentTab />
);
const Upcoming = () => (
    <UpcomingTab />
);
const Invites = () => (
    <InvitesTab />
);
const History = () => (
    <View style={[styles.container, { backgroundColor: '#673ab7' }]} />
);

export default class Home extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'current', title: 'Current' },
            { key: 'upcoming', title: 'Upcoming' },
            { key: 'invites', title: 'Invites' },
            { key: 'history', title: 'History' },
        ],
    };

    _handleIndexChange = index => this.setState({ index });

    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);

        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const backgroundColor = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                            inputIndex => (inputIndex === i ? '#000' : '#fff')
                        ),
                    });
                    const color = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                            inputIndex => (inputIndex === i ? '#fff' : '#222')
                        ),
                    });
                    return (
                        <TouchableOpacity
                            key={i}
                            style={styles.tabItem}
                            onPress={() => this.setState({ index: i })}>
                            <Animated.Text style={[{ fontSize: 14,padding:10,borderRadius:5,overflow:'hidden' }, { color },{backgroundColor}]}>{route.title}</Animated.Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    _renderScene = SceneMap({
        current: Current,
        upcoming: Upcoming,
        invites: Invites,
        history: History
    });

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={this._renderScene}
                renderTabBar={this._renderTabBar}
                onIndexChange={this._handleIndexChange}
                
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        paddingTop: 50,
        backgroundColor: '#fff',
        paddingLeft:10,
        paddingRight:10,
        height:100,
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
    },
});
