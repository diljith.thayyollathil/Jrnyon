import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

class TourDetails extends React.Component{
    render(){
        let {container, tourName, tourCode} = styles;
        return(
            <View style={container}>
                <Text style={tourName}>{this.props.tourname}</Text>
                <Text style={tourCode}>{this.props.tourcode}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        padding:10
    },
    tourName:{
        fontWeight:'800',
        fontSize:20,
        fontFamily: 'Merriweather-Italic'
    },
    tourCode:{
        color:'#767676',
        fontFamily: 'Merriweather-Italic',
        marginTop:10,
        marginBottom:10
    }
})
export default TourDetails;