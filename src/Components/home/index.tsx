import React, { Component } from 'react';
import { View, Text ,Image} from 'react-native';
import BottomNavigation, { FullTab } from 'react-native-material-bottom-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';

import Mytrips from './mytrips';
import AllTrips from './allTrips';


export interface HomeProps {

}
export interface HomeState {

}

class Home extends Component<HomeProps, HomeState> {
  tabs = [
    {
      key: 'all',
      icon: 'align-justify',
      label: 'All',
      barColor: '#000',
      pressColor: 'rgba(255, 255, 255, .1)'
    },
    {
      key: 'mytrips',
      icon: 'briefcase',
      label: 'My Trips',
      barColor: '#000',
      pressColor: 'rgba(255, 255, 255, .1)'
    },
    {
      key: 'profile',
      icon: 'user',
      label: 'Profile',
      barColor: '#000',
      pressColor: 'rgba(255, 255, 255, .1)'
    }
  ];
  constructor(props: HomeProps) {
    super(props);
    this.state = {
      activeTab: {
        key: 'all',
        icon: 'align-justify',
        label: 'All',
        barColor: '#000',
        pressColor: 'rgba(255, 255, 255, .1)'
      },
    }
    this.updateCurrentStore = this.updateCurrentStore.bind(this);
  }
  componentDidMount() {
    this.props.actions.setIpAddress()
    this.props.actions.fetchalltrips();
  }
  updateCurrentStore(){
    this.props.actions.fetchalltrips();
  }
  renderIcon(icon, e) {
    return (
      <Icon size={24} color="white" name={icon} />
    );
  }
  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon.bind(this, tab.icon)}
    />
  );
  render() {
    let data = this.props.allTrips!=undefined ? this.props.allTrips:[];
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          {this.state.activeTab.key === 'all' && <AllTrips allTrips={data} updateCurrentStore={this.updateCurrentStore}/>}
          {this.state.activeTab.key === 'mytrips' && <Mytrips />}
          {this.state.activeTab.key === 'profile' && <View style={{position:'absolute',left:0,right:0,bottom:0,top:0,backgroundColor:'#fff',justifyContent:'center',alignItems:'center'}}>
          <Image stylr={{height:50,width:50}} source={require('../resourses/img/profile.png')} />
          </View>}
        </View>
        <BottomNavigation
          onTabPress={activeTab => {
            this.setState({ activeTab });
          }}
          renderTab={this.renderTab.bind(this)}
          tabs={this.tabs}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Home)
