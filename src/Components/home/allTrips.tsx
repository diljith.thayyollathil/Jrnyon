import * as React from 'react';
import { View, Text, Animated, TouchableOpacity, StyleSheet } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

import AllTab from './allTab';
import EnquireTab from './enquireTab';
import FavouriteTab from './favouriteTab';

export interface tabItems {
    all: string;
    title: string;
}

export interface AllTripProps {
    allTrips: Trip[]
}
export interface AllTripState {
    index: number;
    routes: tabItems[];
}

export default class AllTrips extends React.Component<AllTripProps, AllTripState> {
    state = {
        index: 0,
        routes: [
            { key: 'all', title: 'All Trips' },
            { key: 'enquires', title: 'Enquiries' },
            { key: 'favourites', title: 'Favourites' }
        ],
    };
    
    _handleIndexChange = (index: number) => this.setState({ index });

    _renderTabBar = (props: any) => {
        const inputRange = props.navigationState.routes.map((x: any, i: number) => i);

        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route: any, i: number) => {
                    const backgroundColor = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                            inputIndex => (inputIndex === i ? '#000' : '#fff')
                        ),
                    });
                    const color = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                            inputIndex => (inputIndex === i ? '#fff' : '#222')
                        ),
                    });

                    return (
                        <TouchableOpacity
                            key={i}
                            style={styles.tabItem}
                            onPress={() => this.setState({ index: i })}>
                            <Animated.Text style={[{ fontSize: 14, padding: 10, borderRadius: 5, overflow: 'hidden' }, { color }, { backgroundColor }]}>{route.title}</Animated.Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
    _renderScene = ({ route }) => {
        switch (route.key) {
            case 'all':
                return <AllTab allTrips={this.props.allTrips} />;
            case 'enquires':
                return <EnquireTab />;
            case 'favourites':
                return <FavouriteTab  updateCurrentStore={this.props.updateCurrentStore}/>;
            default:
                return null;
        }
    };

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={this._renderScene}
                renderTabBar={this._renderTabBar}
                onIndexChange={this._handleIndexChange}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        paddingTop: 50,
        backgroundColor: '#fff',
        paddingLeft: 50,
        paddingRight: 50,
        height: 100,
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
    },
});
