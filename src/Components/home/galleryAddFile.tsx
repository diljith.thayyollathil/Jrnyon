import React from 'react';
import {Text,View,StyleSheet, TouchableOpacity} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { Icon } from 'react-native-material-ui';

// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';

class GalleryADD extends React.Component{

    onButtonPressed(){
        // this.props.cameraSelector();
        let images:any = [];
        ImagePicker.openPicker({
            includeBase64:true,
            multiple:true
          }).then(imagefile => {
            imagefile.map((item,i)=>{
                console.log(item,"************")
                let newdata = 'data:'+item.mime+";base64,"+item.data.toString('base64');
                let temp:any ={};
                temp['mime'] = item.mime;
                temp['data'] = newdata;
                images.push(temp);
            })
            let item = {
                id:this.props.id,
                images:images
            }
            this.props.actions.upload_to_gallery(item).then(()=>{
                this.props.updateFront();
            })
          });
    }
    render(){
        const {buttonContainer,AddIcon} = styles;
        return(
            <View style={[buttonContainer,{top:60}]}>
                <TouchableOpacity onPress={this.onButtonPressed.bind(this)}>
                   <Icon  size={25} name="add"></Icon>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    buttonContainer:{
        height:50,
        width:50,
        backgroundColor:'#FFAA5D',
        position:'absolute',
        right:20,
        zIndex:9,
        borderRadius:28,
        alignItems:'center',
        justifyContent:'center'
    },
    AddIcon:{
        fontSize:25,
    }
})
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(GalleryADD)