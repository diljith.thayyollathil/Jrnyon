import React from 'react';
import {Text,View,StyleSheet, TouchableOpacity} from 'react-native';
import { Icon } from 'react-native-material-ui';

class FloatingButton extends React.Component{
    onButtonPressed(){
        console.log("clicked")
       
    }
    render(){
        const {buttonContainer,AddIcon} = styles;
        return(
            
            <View style={[buttonContainer,{top:35}]}>
                <TouchableOpacity onPress={this.props.onButtonPressed}>
                    <Icon  size={25} name="add"></Icon>
                </TouchableOpacity>
            </View>
            
        )
    }
}
const styles=StyleSheet.create({
    buttonContainer:{
        height:50,
        width:50,
        backgroundColor:'#FFAA5D',
        position:'absolute',
        right:20,
        zIndex:9,
        borderRadius:28,
        alignItems:'center',
        justifyContent:'center'
    },
    AddIcon:{
        fontSize:25,
    }
})
export default FloatingButton;