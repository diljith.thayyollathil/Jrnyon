import React from 'react';
import { View, Text,ScrollView } from 'react-native';
import TripIndividual from './tripindividual';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';

export interface TripProps {

}
export interface TripState {

}
class EnquireTab extends React.Component<TripProps, TripState>{
    constructor(props: TripProps) {
        super(props);
        this.state = {

        }
    }
    componentDidMount(){
        this.props.actions.fetchallenquiries()
    }
    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                {this.props.enquires.trips!=undefined && this.props.enquires.trips.map((item,i)=>{
                    return(
                        <TripIndividual enquireFlag={true}  invited={false} trip={item} key={i} />
                    )
                })}
            </ScrollView>
        )
    }
}
const mapStateToProps = state => {
    return state;
  }
  const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });
  
  export default connect(mapStateToProps, mapDispatchToProps)(EnquireTab)

