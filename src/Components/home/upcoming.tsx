import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

class UpcomingTab extends React.Component {
    constructor(props:any) {
        super(props);
        this.state = {
            time: '',
        }
    }
    componentDidMount() {
        this.interval = setInterval(() => {
            let countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();
            let now = new Date().getTime();
            let distance = countDownDate - now;

            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            let time = this.state.time;
            time = days + "d " + " : " + hours + "h " + " : " + minutes + "m " + " : " + seconds + "s ";
            this.setState({ time });
        }, 1000);

        this.props.actions.fetch_single_upcoming_trips()
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    ViewDetails() {
        Actions.all_upcoming();
    }
    render() {
        const { container, headingContent, tripBegins, liveTime,
            targetDate, ViewDetailsButton, ViewDetailsRow } = styles;
        return (
            <View style={container}>
                <Image source={require('../resourses/img/background.png')} style={styles.backgroundImage} />
                <View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: 'rgba(0,0,0,0.5)', alignItems: 'center' }}>
                    <Text style={headingContent}>
                        {this.props.upComing.trip!=undefined ? this.props.upComing.trip.name:'-'}
                    </Text>
                    <Text style={tripBegins}>Your Trip Begins In</Text>
                    <Text style={liveTime}>{this.state.time}</Text>
                    <View style={targetDate}>
                        <Icon size={18} color="#FFAA5D" name='calendar' />
                        <Text style={{ color: '#FFAA5D', fontWeight: '700', marginTop: 3, marginLeft: 10 }}>{this.props.upComing.trip!=undefined ? this.props.upComing.trip.start_date_in_format1:'-'}</Text>
                    </View>
                    <View style={ViewDetailsRow}>
                        <TouchableOpacity onPress={this.ViewDetails.bind(this)}>
                            <View style={ViewDetailsButton}>
                                <Text style={{ fontWeight: '800', width: 180, textAlign: 'center' }}>
                                    VIEW DETAILS
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 20, width: '100%', justifyContent: 'center', alignItems: 'center', }}>
                        <TouchableOpacity onPress={this.ViewDetails.bind(this)}>
                            <Text style={{ color: '#fff' }}>
                                SEE ALL
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const win = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    liveTime: {
        color: '#fff',
        fontWeight: '700',
        fontSize: 25,
        fontFamily: 'Merriweather-Italic'
    },
    targetDate: {
        width: 200,
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 1, borderBottomWidth: 1,
        borderTopColor: '#FFAA5D', borderBottomColor: '#FFAA5D',
        padding: 10,
        marginTop: 30,
        marginBottom: 30
    },
    backgroundImage: {
        flex: 1,
        width: win.width,
        height: win.height
    },
    headingContent: {
        fontWeight: '800',
        color: '#fff',
        width: 350,
        padding: 40,
        fontSize: 18,
        marginTop: 30,
        textAlign: 'center',
        fontFamily: 'Merriweather-Italic'
    },
    tripBegins: {
        color: '#767676',
        width: '100%',
        textAlign: 'center',
        padding: 20
    },
    ViewDetailsRow: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    ViewDetailsButton: {
        width: 180,
        height: 45,
        borderRadius: 10,
        backgroundColor: '#FFAA5D',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
        marginBottom: 20
    }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(UpcomingTab);
