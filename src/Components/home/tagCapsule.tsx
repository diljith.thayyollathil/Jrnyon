import React from 'react';
import {View,Text,StyleSheet} from 'react-native';

class TagCapsule extends React.Component{
    render(){
        const {tagscontainer} = styles;
        return(
            <View style={tagscontainer}>
                <Text style={{minWidth:50,textAlign:'center'}}>{this.props.tagItem}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    tagscontainer:{
        height:28,
        backgroundColor:'#FFEAD2',
        borderRadius:10,
        marginRight:10,
        padding:5,
        paddingLeft:10,
        paddingRight:10,
        alignItems:'center'
    }
})
export default TagCapsule;