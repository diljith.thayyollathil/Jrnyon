import React from 'react';
import { View, Text,ScrollView } from 'react-native';
import TripIndividual from './tripindividual';


export interface TripProps {
    allTrips:Trip[];
}
export interface TripState {

}
class AllTab extends React.Component<TripProps, TripState>{
    constructor(props: TripProps) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                {this.props.allTrips.trips!=undefined && this.props.allTrips.trips.map((item:Trip,i:number)=>{
                    return(
                        <TripIndividual alltripFlag={true} invited={false} trip={item} key={i} />
                    )
                })}
            </ScrollView>
        )
    }
}
export default AllTab;
