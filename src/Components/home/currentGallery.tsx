import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import {View,Text,Image, StyleSheet, Dimensions, TouchableOpacity} from 'react-native';
import { Actions } from 'react-native-router-flux';

const GalleryOverlay = ({length}) =>{
    const win = Dimensions.get('window');
    return(
        <View >
            {/* <Text style={{color:'#fff',fontWeight:'600'}}>+{length-6}</Text> */}
            <Text style={{color:'#fff',fontSize:14,fontWeight:'800',width:70}}>{length} Photos</Text>
        </View>
    );
}


class Gallery extends React.Component{
    async moveToGallery(){
        await this.props.actions.setGalleryId({id:this.props.id});
        Actions.gallery({id:this.props.id});
    }
    render(){
        const {container, galleryThumbnail} = styles;
        let length = this.props.tourImages!=undefined ? this.props.tourImages.length: 0 ;
        return(
            <View style={container}>
                {this.props.tourImages!=undefined && this.props.tourImages.map((item,i)=>{
                    let urll = item;
                    return(
                        <View key={i}>
                            {i<6 && 
                                <TouchableOpacity onPress={this.props.showImageInFull.bind(this,urll)}>
                                <Image style={galleryThumbnail} source={{uri:item}}/>
                                </TouchableOpacity>}
                                {(i===5) && <TouchableOpacity style={{height:110,width:(win.width/3)-2,position:'absolute',top:2,
                                    backgroundColor:'rgba(0,0,0,0.4)',flexDirection:'column',alignItems:'center',justifyContent:'center'}} onPress={this.moveToGallery.bind(this)}>
                                <GalleryOverlay length={length} /></TouchableOpacity>}
                            
                           
                        </View>
                    )
                })}

            </View>
        )
    }
}

const win = Dimensions.get('window');
const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },
    galleryThumbnail:{
        height:110,
        width:(win.width/3)-2,
        marginRight:1,
        marginLeft:1,
        marginTop:2
    }
})
const mapStateToProps = state => {
    return state;
  }
  const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });
  export default connect(mapStateToProps, mapDispatchToProps)(Gallery)
