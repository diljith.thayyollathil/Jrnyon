import React from 'react';
import { View, Image, Text, StyleSheet, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import StarRating from 'react-native-star-rating';
import TagCapsule from './tagCapsule';
import Icon from 'react-native-vector-icons/FontAwesome';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';

import { Actions } from 'react-native-router-flux';


export interface IndividualTripProps {
    invited:boolean;
    trip:Trip;
    favouriteFlag:boolean;
}
export interface IndividualTripState {
    color:string;
    time:string;
    joinModal:boolean
}


class TripIndividual extends React.Component<IndividualTripProps,IndividualTripState> {
    constructor(props:IndividualTripProps) {
        super(props);
        this.state = {
            color: '#fff',
            time: '',
            joinModal:false
        }
    }
    async addToFavourites(item:Trip) {
        let color = this.state.color;
        if (color === '#ff5f58') {
            let itemdetails = {
                action: 'remove',
                data:item
            }
            await this.props.actions.add_to_favourites(itemdetails)
            await this.props.actions.remove_from_favourites(item);
            
            if(this.props.favouriteFlag==true){
                await this.props.updateCurrentStore();
                console.log(this.props,"inside individual component");
                await this.props.actions.fetchallenquiries();
            }
            if(this.props.favouriteFlag!=true){
                this.setState({ color: '#fff' });
            }
           
        } else {
            let itemdetails = {
                action: 'add',
                data:item
            }
            this.props.actions.add_to_favourites(itemdetails)
            this.setState({ color: '#ff5f58' })
        }
    }
    componentDidMount(){
        if(this.props.trip.has_favourited==true){
            this.setState({color:'#ff5f58'})
        }
        if(this.props.alltripFlag==true){
            if(this.props.trip.has_favourited==true){
                this.setState({color:'#ff5f58'})
            }else{
                this.setState({color:'#fff'})
            }
        }
        if(this.props.enquireFlag==true){
            if(this.props.trip.has_favourited==true){
                this.setState({color:'#ff5f58'})
            }else{
                this.setState({color:'#fff'})
            }
        }

        if(this.props.favouriteFlag===true){
            this.setState({color:'#ff5f58'})
        }
        
       
        if(this.props.type=='allupcoming'){
            this.interval = setInterval(() => {
                let countDownDate = new Date(this.props.startDate).getTime();
                let now = new Date().getTime();
                let distance = countDownDate - now;
    
                let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                let time = this.state.time;
                time = days + "d " + " : " + hours + "h " + " : " + minutes + "m " + " : " + seconds + "s ";
                this.setState({ time });
            }, 1000);
        }
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    openFullDetails(id:number, e:any) {
        if (this.props.invited === true) {
            Actions.tripDetails_invited({ id: id });
        } else {
            Actions.tripDetails({ id: id });
        }
    }
    render() {
        const { container, locationStyle, triprowContainer, tripTitle,
            packageRate, rupeeSymbol, starRatingRow, favouriteIcon, favouriteIconContainer,
            invitedWrapper } = styles;
        let CoverURL = this.props.ipadress.ipAdress+"/media/" + this.props.trip.images[0];
        return (
            <View style={container}>
                <TouchableOpacity onPress={this.openFullDetails.bind(this, this.props.trip.id)}>
                {(this.props.type != 'invites' && this.props.type!='allupcoming') &&<TouchableOpacity style={favouriteIconContainer} onPress={this.addToFavourites.bind(this,this.props.trip)}><Icon size={16} style={favouriteIcon} color={this.state.color} name='heart' /></TouchableOpacity>}
                    <Image style={locationStyle} source={{ uri: CoverURL }} />
                    {this.props.type=='allupcoming' && <View style={{position:'absolute',height:160,left:0,right:0,backgroundColor:'rgba(0,0,0,0.5)',justifyContent:'center',alignItems:'center'}}>
                        <View>
                            <Text style={{color:'#fff',fontSize:12}}>Your trip begins in</Text>
                            <Text style={{color:'#fff',fontSize:30}}>{this.state.time}</Text>
                        </View>
                        </View>}
                    {this.props.type==='invites' && <View style={invitedWrapper}>
                        <Icon name='envelope-open' size={20} style={{color:'#fdac4c',padding:5,paddingLeft:10,paddingRight:10,paddingTop:8, shadowColor: '#ccc',shadowOpacity: 0.5,shadowRadius: 5,shadowOffset: {width: 0, height: 1}}} />
                        <View style={{borderLeftWidth:1,borderLeftColor:'#efefef',height:40,width:2}}></View>
                        <Text style={{fontWeight:'800',fontSize:12,width:130,paddingTop:4,paddingLeft:10}}>Admin <Text style={{fontWeight:'300'}}>has invited you to join them</Text></Text>
                    </View>}
                    <View style={[triprowContainer,{marginTop:10}]}>
                        <View style={{ width: '78%', flex: 1 }}>
                            <Text style={tripTitle}>{this.props.trip.name}</Text>
                        </View>
                        <View style={{ width: '22%', alignItems: 'flex-end' }}>
                        {this.props.type==='invites' && 
                            <TouchableOpacity style={{marginRight:10}} onPress={this.props.openJoinModal.bind(this,this.props.trip.id)}> 
                                {this.props.trip.has_joined==true && <Text style={{color:'green', fontSize:12}}>JOINED</Text>}
                                {this.props.trip.has_rejected==true && <Text style={{color:'red', fontSize:12}}>REJECTED</Text>}
                                {(this.props.trip.has_joined==false && this.props.trip.has_rejected==false) &&<Text style={{color:'#fdac4c',fontSize:12}}>RESPOND</Text>}
                            </TouchableOpacity>}
                        </View>
                    </View>
                    <View style={[triprowContainer, { marginTop: -3 }]}>
                        <View style={starRatingRow}>
                            <Image style={rupeeSymbol} source={require('../resourses/img/rupee.jpg')} />
                            <Text style={[packageRate]}>{this.props.trip.amount}</Text>
                        </View>
                        <View style={{ width: 10, height: 16, borderWidth: 2, borderColor: '#fff', borderRightColor: '#ccc', marginTop: 13, marginRight: 20, marginLeft: -3 }}></View>
                        <View style={{ paddingTop: 12 }}>
                        <StarRating
                            disabled={true}
                            maxStars={5}
                            rating={this.props.trip.rating}
                            starSize={15}
                            fullStarColor="#ffab5c"
                        />
                        </View>
                        <Text style={{ marginTop: 11, color: '#757575' }}>({this.props.trip != undefined ? this.props.trip.rating : '-'})</Text>
                    </View>
                    
                    <View style={[triprowContainer, { marginBottom: 20 }]}>
                        <ScrollView horizontal={true}>
                            {this.props.trip.tags.map((tagItem, i) => {
                                return (
                                    <TagCapsule tagItem={tagItem} key={i} />
                                )
                            })}
                        </ScrollView>
                    </View>
                </TouchableOpacity>
                
            </View>
        )
    }
}
const win = Dimensions.get('window');
const styles = StyleSheet.create({
    invitedWrapper:{
        width:180,
        height:40,
        position:'absolute',
        right:0,
        top:60,
        backgroundColor:'#fff',
        flexDirection:'row',
        borderTopLeftRadius:8,
        borderBottomLeftRadius:8,
        overflow:'hidden'
        
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginBottom:10
    },
    favouriteIconContainer: {
        position: 'absolute',
        right: 10,
        top: 20,
        height:30,
        width:30,
        zIndex: 1,
        backgroundColor:'rgba(0,0,0,0.4)',
        borderRadius:30,
        alignItems:'center',
        justifyContent:'center',
    },
    favouriteIcon: {
        paddingLeft:2
    },
    packageRate: {
        fontWeight: '700',
        color: '#767676',
        fontSize: 16,
        fontFamily: 'Merriweather-Regular',
    },
    locationStyle: {
        flex: 1,
        alignSelf: 'stretch',
        width: win.width,
        height: 180,
        marginTop: 10,
        marginBottom: 10,
        zIndex: 0
    },
    triprowContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 10
    },
    tripTitle: {
        fontSize: 14,
        fontFamily: 'Merriweather-Bold',
    },
    rupeeSymbol: {
        height: 13,
        width: 13,
        marginTop: 5,
        opacity: .7
    },
    starRatingRow: {
        flexDirection: 'row',
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10
    }
})
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(TripIndividual);