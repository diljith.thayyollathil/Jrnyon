import React from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Modal, Image, Dimensions } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import TourDetails from './tourDetails';
import Gallery from './currentGallery';
import TagCapsule from './tagCapsule';
import { Icon } from 'react-native-material-ui';
import GalleryADD from './galleryAddFile';
import ResponsiveImage from 'react-native-responsive-image';
import ImagePicker from 'react-native-image-crop-picker';

import { Actions } from 'react-native-router-flux';

class Current extends React.Component {
    constructor(props: any) {
        super(props);
        this.state = {
            showImage: false,
            imageUrl: '',
            selectUploader: false,
        }
    }
    componentDidMount() {
        this.props.actions.fetch_current_trip();
    }
    updateFront() {
        this.props.actions.fetch_current_trip();
        this.setState({ selectUploader: false });
    }
    showImageInFull(data) {
        this.setState({ imageUrl: data });
        this.setState({ showImage: true });
    }
    closeModal() {
        this.setState({ showImage: false });
    }
    cameraSelector() {
        this.setState({ selectUploader: true });
    }
    openCameraForFIles() {
        let images: any = [];
        ImagePicker.openCamera({
            includeBase64: true,
            cropping: true
        }).then(imagefile => {
            console.log(imagefile)
            let newdata = 'data:' + imagefile.mime + ";base64," + imagefile.data.toString('base64');
            let temp: any = {};
            temp['filename'] = imagefile.filename;
            temp['data'] = newdata;
            images.push(temp);
            let item = {
                id: this.props.id,
                images: images
            }
            this.props.actions.upload_to_gallery(item).then(() => {
                this.updateFront();
            })
        });
    }
    openGalleryForFIles() {
        let images: any = [];
        ImagePicker.openPicker({
            includeBase64: true,
            multiple: true
        }).then(imagefile => {
            imagefile.map((item, i) => {
                let newdata = 'data:' + item.mime + ";base64," + item.data.toString('base64');
                let temp: any = {};
                temp['filename'] = item.filename;
                temp['data'] = newdata;
                images.push(temp);
            })
            let item = {
                id: this.props.id,
                images: images
            }
            this.props.actions.upload_to_gallery(item).then(() => {
                this.updateFront();
            })
        });
    }
    render() {
        const win = Dimensions.get('window');
        let { container, tripRow } = styles;
        return (
            <View style={{ flex: 1 }}>
                {this.props.current.trips.name != undefined && <ScrollView>
                    <View style={container}>
                        <TourDetails tourname={this.props.current.trips.name} tourcode={this.props.current.trips.trip_code} />
                        <GalleryADD cameraSelector={this.cameraSelector.bind(this)} updateFront={this.updateFront.bind(this)} id={this.props.current.trips.id} />
                        <Gallery ip={this.props.ipadress.ipAdress} showImageInFull={this.showImageInFull.bind(this)} id={this.props.current.trips.id} tourImages={this.props.current.trips.length != 0 ? this.props.current.trips.user_images : []} />

                        <View style={{ flex: 1, marginTop: 20 }}>
                            <View style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }]}>
                                <Image style={{ height: 40, width: 40, marginRight: 10 }} source={require('../resourses/img/itinerary.png')} />
                                <TouchableOpacity style={{ width: '100%', alignItems: 'flex-start' }} onPress={() => { Actions.itnerary_timeline({ id: this.props.current.trips.id }) }}><Text style={{ fontSize: 13, textAlign: 'center' }}>Itinerary</Text></TouchableOpacity>
                            </View>
                            <View style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }]}>
                                <Image style={{ height: 40, width: 40, marginRight: 10 }} source={require('../resourses/img/travelers_invitees.png')} />
                                <TouchableOpacity style={{ width: '100%', alignItems: 'flex-start' }} onPress={() => { Actions.invitees({ id: this.props.current.trips.id, nameOfInvitee: this.props.current.trips.invited_by, nameOfTrip: this.props.current.trips.name }) }}><Text style={{ fontSize: 13, textAlign: 'center' }}>Invitees</Text></TouchableOpacity>
                            </View>
                            <View style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', marginBottom: 60, flexDirection: 'row' }]}>
                                <Image style={{ height: 40, width: 40, marginRight: 10 }} source={require('../resourses/img/more_info.png')} />
                                <Text style={{ fontSize: 13, textAlign: 'center' }}>Detailed Information</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showImage}
                    onRequestClose={() => { this.setState({ showImage: false }) }}>
                    <View style={styles.containerStyle}>
                        <View style={styles.CardSectionStyle}>
                            <TouchableOpacity onPress={() => { this.setState({ showImage: false }) }}>
                                <Icon size={20} name='close' style={{ alignSelf: 'flex-end', marginTop: 5, marginBottom: 10, marginRight: 5, color: '#fff' }} />
                            </TouchableOpacity>
                            <ResponsiveImage source={{ uri: this.state.imageUrl }} initWidth={win.width - 5} initHeight="248" />
                        </View>
                    </View>
                </Modal>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.selectUploader}
                    onRequestClose={() => { this.setState({ showImage: false }) }}>
                    <View style={styles.containerStyle}>
                        <View style={[styles.CardSectionStyle, { justifyContent: 'center', alignItems: 'center' }]}>
                            <View style={{ backgroundColor: '#fff', height: 100, width: 200 }}>
                                <TouchableOpacity onPress={this.openCameraForFIles.bind(this)} style={{ padding: 15, flex: 1, borderBottomWidth: 1, borderBottomColor: '#efefef' }}>
                                    <Text>Take Photo</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.openGalleryForFIles.bind(this)} style={{ padding: 15, flex: 1 }}>
                                    <Text>Choose from library</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    tripRow: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        padding: 20,
        flexDirection: 'row'
    },
    CardSectionStyle: {
        padding: 10,
        backgroundColor: 'rgba(0,0,0,0)',
        position: 'relative',
        flexDirection: 'column',
        paddingBottom: 20,
    },
    containerStyle: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        position: 'relative',
        flex: 1,
        padding: 10,
        justifyContent: 'center',
    }
})
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Current)