import React from 'react';
import { View, Text,ScrollView, TouchableOpacity, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import TripIndividual from './tripindividual';

export interface TripProps {

}
export interface TripState {
    joinModal:boolean;
    id:number
}
class InvitesTab extends React.Component<TripProps, TripState>{
    constructor(props: TripProps) {
        super(props);
        this.state = {
            joinModal:false,
        }
    }
    componentWillMount(){
        this.props.actions.fetch_myTrips_invites()
    }
    joinedTrip(){
        this.props.actions.JoinTrip(this.state.id);
        this.setState({joinModal:false});
    }
    rejectTrip(){
        this.props.actions.RejectTrip(this.state.id);
        this.setState({joinModal:false});
    }
    openJoinModal(data,e){
        this.setState({id:data});
        this.setState({joinModal:true});
    }
    render() {
        return (
            <View style={{flex:1}}>
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                {this.props.myTrips.trips!=undefined && this.props.myTrips.trips.map((item,i)=>{
                    return(
                        <TripIndividual ip={this.props.ipadress.ipAdress} openJoinModal={this.openJoinModal.bind(this)} type='invites' invited={true} trip={item} key={i} />
                    )
                })}
            </ScrollView>
            {this.state.joinModal == true && <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, top: 0, backgroundColor: 'rgba(0,0,0,0.5)', flex: 1, zIndex: 9999 }}>
                    <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, flex: 1, height: 237, backgroundColor: '#fff', zIndex: 2 }}>
                        <Text style={{ fontSize: 20, fontFamily: "Merriweather-italic", textAlign: 'center', padding: 20 }}>Join This Trip</Text>
                        <View style={{ alignContent: 'center', alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            <Image style={{ width: 50, height: 50, marginRight: 10 }} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAb2GoiLTaOTuNxW9s4qz1afqhFc3swOwfw1kMenJ83zXqJBrW' }} />
                            <Image style={{ width: 50, height: 50, marginRight: 10 }} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfdikQHNtpxpufc3B7lHxRWNviqkQsXlprIbOy2N7ZHJhgVz5J' }} />
                            <Image style={{ width: 50, height: 50, marginRight: 10 }} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAb2GoiLTaOTuNxW9s4qz1afqhFc3swOwfw1kMenJ83zXqJBrW' }} />
                        </View>
                        <Text style={{ textAlign: 'center', color: '#767676', fontSize: 13, padding: 20, paddingLeft: 58, paddingRight: 58 }}>Join John Doe , Jane Doe and 12 others on this trip</Text>
                        <View style={{flex:1,flexDirection:'row'}}>
                        <TouchableOpacity onPress={this.joinedTrip.bind(this)} style={{ width:'50%',height: 50, borderTopWidth: 1, borderTopColor: '#ccc', paddingTop: 17,backgroundColor:'#FFAA5D' }} >
                            <Text style={{ textAlign: 'center', color: '#767676' }}>Join this trip</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.rejectTrip.bind(this)} style={{width:'50%', height: 50, borderTopWidth: 1, borderTopColor: '#ccc', paddingTop: 17 }} >
                            <Text style={{ textAlign: 'center', color: '#767676' }}>No. not now</Text>
                        </TouchableOpacity>
                        </View>
                    </View>
                </View>}
            </View>
        )
    }
}
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(InvitesTab)
