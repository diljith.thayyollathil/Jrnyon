import React from 'react';
import {View,Text, Image,TouchableOpacity} from 'react-native';
import { Actions } from 'react-native-router-flux';

class Error extends React.Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                    <Image source={require('../resourses/img/back.png')} style={{position:'absolute',top:20,left:20,height:30,width:30}} />
                </TouchableOpacity>
                <Text>{this.props.message}</Text>
            </View>
        )
    }
}
export default Error;