import React from 'react';
import {View,Text} from 'react-native';

class SortBar extends React.Component{
    render(){
        return(
            <View style={{backgroundColor:'#c6c6c6',flex:1,height:35,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                <Text style={{fontSize:11}}>Sort By </Text>
                <Text style={{fontSize:10,color:'#3d3d3d',backgroundColor:'#fff',borderRadius:10,overflow:'hidden',padding:3,paddingLeft:10,paddingRight:10}}>Recently Uploaded</Text>
            </View>
        )
    }
}
export default SortBar;