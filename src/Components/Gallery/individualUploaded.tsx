import React from 'react';
import { View, Text, Image, ScrollView, Animated, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { CustomHeaderGallery } from '../common';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import Gallery from 'react-native-image-gallery';
import { Icon } from 'react-native-material-ui';

class IndividualUploadings extends React.Component {
    constructor(props: any) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0),
            showGallery: false,
            activeIndex: 0
        }
    }
    componentDidMount() {
        this.props.actions.gallery_individual({ userId: this.props.id, id: this.props.userId })
    }
    openGallery(data) {
        this.setState({ showGallery: true });
        this.setState({ activeIndex: data });
    }
    render() {
        const { fill } = styles;
        const HEADER_SCROLL_DISTANCE = 150 - 80;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [150, 80],
            extrapolate: 'clamp',
        });

        const floatingPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [131, 69],
            extrapolate: 'clamp',
        });

        const backSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 25],
            extrapolate: 'clamp',
        });

        const backPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [65, -10],
            extrapolate: 'clamp',
        });
        const Titleposition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [55, 20],
            extrapolate: 'clamp',
        });
        const TitleSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [25, 18],
            extrapolate: 'clamp',
        });
        const TitleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [10, 0],
            extrapolate: 'clamp',
        });
        const TripcodeMargin = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [5, 5],
            extrapolate: 'clamp',
        });
        const TripcodeFontSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [14, 10],
            extrapolate: 'clamp',
        });
        const TagCapsuleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [32, 28],
            extrapolate: 'clamp',
        });
        const TagCapsulePadding = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [7, 3],
            extrapolate: 'clamp',
        });
        const TagCapsuleFOntSIze = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [12, 10],
            extrapolate: 'clamp',
        });
        let dataHeader = { id: this.props.userId }
        let galleryView = [];
        if (this.props.Gallery.individualGallery != undefined) {
            this.props.Gallery.individualGallery.map((data, k) => {
                galleryView.push({ source: { uri:data } });
            })
        }
        const win = Dimensions.get('window');
        return (
            <View style={styles.fill}>
                <ScrollView
                    style={styles.fill}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
                    }>
                    <View style={{ marginTop: 150 }}>
                        <View style={{ flex: 1, backgroundColor: '#ccc', height: 35 }}>

                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                            {this.props.Gallery.individualGallery != undefined && this.props.Gallery.individualGallery.map((item, i) => {
                                return (
                                    <TouchableOpacity onPress={this.openGallery.bind(this, i)} key={i}>
                                        <Image key={i} style={styles.galleryThumbnail} source={{ uri:  item }} />
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                    </View>
                </ScrollView>
                {this.state.showGallery && <View style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, backgroundColor: 'rgba(0,0,0,0.9)', zIndex: 99999 }}>
                    <TouchableOpacity style={{ position: 'absolute', top: 10, right: 10, zIndex: 9999999 }} onPress={() => { this.setState({ showGallery: false }) }}>
                        <Icon size={20} name='close' style={{ alignSelf: 'flex-end', marginTop: 5, marginBottom: 10, marginRight: 5, color: '#fff' }} />
                    </TouchableOpacity>
                    <Gallery

                        initialPage={this.state.activeIndex}
                        style={{ flex: 1, backgroundColor: 'black', height: win.height }}
                        images={galleryView}
                    /></View>}
                <CustomHeaderGallery headerHeight={headerHeight}
                    backPosition={backPosition}
                    backSize={backSize}
                    individualGallery={true}
                    idforGallery={dataHeader}
                    HeaderTitle={this.props.user}
                    Titleposition={Titleposition}
                    TitleSize={TitleSize}
                    TitleHeight={TitleHeight}
                    TripcodeMargin={TripcodeMargin}
                    TripcodeFontSize={TripcodeFontSize}
                />
            </View>
        )
    }
}
const win = Dimensions.get('window');
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fff'
    },
    galleryThumbnail: {
        height: 110,
        width: (win.width / 3) - 2,
        marginRight: 1,
        marginLeft: 1,
        marginTop: 2
    }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(IndividualUploadings);