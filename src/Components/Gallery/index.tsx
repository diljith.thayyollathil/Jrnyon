import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { ScrollView, View, Animated, StyleSheet,Text } from 'react-native';
import {CustomHeaderGallery, TagRow} from '../common';
import SortBar from './sortBar';
import GalleryBody from './galleryBody';

class Gallery extends React.Component{

    constructor(props: any) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0),
            refresh:true
        }
    }
    async componentDidMount(){
        console.log(this.props,"in gallery view");
        await this.props.actions.fetchGalleryId();
        this.props.actions.gallery_view({id:this.props.Gallery.id});
    }
    componentWillReceiveProps(nextProps:any){
        console.log("consoled", nextProps);
        if(nextProps.Gallery.data=='refresh'){
            this.props.actions.gallery_view({id:nextProps.Gallery.id});
        }
    }
    render(){
        const {fill} = styles;
        const HEADER_SCROLL_DISTANCE = 150 - 80;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [150, 80],
            extrapolate: 'clamp',
        });
        const floatingPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [131, 69],
            extrapolate: 'clamp',
        });

        const backSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 25],
            extrapolate: 'clamp',
        });

        const backPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [65, -10],
            extrapolate: 'clamp',
        });
        const Titleposition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [55, 20],
            extrapolate: 'clamp',
        });
        const TitleSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [25, 18],
            extrapolate: 'clamp',
        });
        const TitleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [10, 0],
            extrapolate: 'clamp',
        });
        const TripcodeMargin = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [5, 5],
            extrapolate: 'clamp',
        });
        const TripcodeFontSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [14, 10],
            extrapolate: 'clamp',
        });
        const TagCapsuleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [32, 28],
            extrapolate: 'clamp',
        });
        const TagCapsulePadding = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [7, 3],
            extrapolate: 'clamp',
        });
        const TagCapsuleFOntSIze = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [12, 10],
            extrapolate: 'clamp',
        });
        console.log(this.props.Gallery,"GGGGGGGGG")
        return(
            <View style={styles.fill}>
                <ScrollView
                    style={styles.fill}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
                    }>
                    <View style={{marginTop:150}}>
                        <SortBar />
                        {this.props.Gallery.trips!=undefined && <GalleryBody id={this.props.Gallery.id} ip={this.props.ipadress.ipAdress} gallery={this.props.Gallery } />}
                        
                   </View>
                </ScrollView>
                <CustomHeaderGallery headerHeight={headerHeight}
                    backPosition={backPosition}
                    backSize={backSize}
                    HeaderTitle="Gallery"
                    gallerypage={true}
                    Titleposition={Titleposition}
                    TitleSize={TitleSize}
                    TitleHeight={TitleHeight}
                    TripcodeMargin={TripcodeMargin}
                    TripcodeFontSize={TripcodeFontSize}
                />
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fff'
    }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);