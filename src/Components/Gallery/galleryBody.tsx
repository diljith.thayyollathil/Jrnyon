import React from 'react';
import {View, Text,Image,StyleSheet, Dimensions, TouchableOpacity,Modal} from 'react-native';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import ResponsiveImage from 'react-native-responsive-image';
import { Icon } from 'react-native-material-ui';

const GalleryOverlay = ({count}) =>{
    const win = Dimensions.get('window');
    return(
        <View style={{justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',fontWeight:'600',fontSize:12,width:30}}>+{count}</Text>
        </View>
    );
}


class GalleryBody extends React.Component{
    constructor(props:any){
        super(props);
        this.state = {
            showPopup:false,
        }
    }
    showImage(url:string){
        this.setState({popupUrl:url});
        this.setState({showPopup:true});
    }
    movetOindividualLibrary(data,user,e){
        Actions.galleryIdividual({id:data,userId:this.props.id,user:user});
    }
    render(){
        return(
            <View>
                {this.props.gallery.trips.map((item:any,i:number)=>{
                    return(
                        <View style={{flex:1}} key={i}>
                            <View style={{flex:1,padding:10}}>
                                <Text style={{alignSelf:'flex-start',color:'#3d3d3d',fontWeight:'800',fontSize:12}}>{item.uploaded_user}</Text>
                                <Text style={{position:'absolute',top:10,right:20,color:'#767676',alignSelf:'flex-end',fontSize:11}}>{moment(item.last_uploaded_on).fromNow()}</Text>
                            </View>
                            <View style={{flex:1,flexDirection:'row',alignItems: 'flex-start',}}>
                                {item.images!=undefined && item.images.map((image:any,k:number)=>{
                                    return(
                                        <View key={k}>
                                        <TouchableOpacity onPress={this.showImage.bind(this,image)}>
                                            <Image source={{uri:image}} style={styles.galleryThumbnail} />
                                        </TouchableOpacity>
                                            {k===3 && <TouchableOpacity style={{height:80,width:(win.width/4)-2,position:'absolute',
                                                backgroundColor:'rgba(0,0,0,0.4)',flexDirection:'column',alignItems:'center',justifyContent:'center'}} onPress={this.movetOindividualLibrary.bind(this,item.user_id,item.uploaded_user)} >
                                            <GalleryOverlay   count={item.images.length-4}/></TouchableOpacity>}
                                        </View>
                                    )
                                })}
                            </View>
                        </View>
                    )
                })}
                     <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.showPopup}
                        onRequestClose={()=> { this.setState({showPopup:false}) }}>
                        <View style={styles.containerStyle}>
                            <View style={styles.CardSectionStyle}>
                                <TouchableOpacity onPress={()=>{this.setState({showPopup:false})}}>
                                    <Icon size={20} name='close' style={{ alignSelf: 'flex-end', marginTop: 5,marginBottom:10, marginRight: 5,color:'#fff' }} />
                                </TouchableOpacity>
                                <ResponsiveImage source={{uri:this.state.popupUrl}} initWidth={win.width-5} initHeight="248"/>
                            </View>
                        </View>
                        </Modal>
            </View>
        )
    }
}
const win = Dimensions.get('window');
const styles = StyleSheet.create({
    galleryThumbnail:{
        height:80,
        width:(win.width/4)-2,
        marginRight:1,
        marginLeft:1,
        marginTop:2
    },
    CardSectionStyle: {
        padding: 10,
        backgroundColor: 'rgba(0,0,0,0)',
        position: 'relative',
        flexDirection: 'column',
        paddingBottom: 20,
    },
    containerStyle: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        position: 'relative',
        flex: 1,
        padding: 10,
        justifyContent: 'center',
    }
})
export default GalleryBody;