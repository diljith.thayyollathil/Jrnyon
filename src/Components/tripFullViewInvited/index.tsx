import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { ScrollView, View, Animated, StyleSheet } from 'react-native';
import {CustomHeader1, TagRow} from '../common';
import TripImage from './tripRepresentImage';
import TripBody from './tripBody';
import RespondRow from './respondSection';
import RespondRowBook from './respondSectionBook';

export interface tripProps {

}
export interface tripClass {

}
class TripViewInvited extends React.Component<tripProps,tripClass>{
    constructor(props: tripProps) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0),
        }
        this.onButtonPressed = this.onButtonPressed.bind(this);
    }
    JoinThisTrip(){
        let id = (this.props.selected.trip.id ? this.props.selected.trip.id:'');
        this.props.actions.JoinTrip(id).then(()=>{
            this.props.actions.fetchTripDetails(id);
        })
    }
    RejectThisTrip(){
        let id = (this.props.selected.trip.id ? this.props.selected.trip.id:'');
        this.props.actions.RejectTrip(id).then(()=>{
            this.props.actions.fetchTripDetails(id);
        })
    }
    componentDidMount(){
        let id = this.props.id;
        this.props.actions.fetchTripDetails(id);
    }
    onButtonPressed() {
        console.log('sdsa');
     }
    render(){
        const {fill} = styles;
        const HEADER_SCROLL_DISTANCE = 150 - 80;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [150, 80],
            extrapolate: 'clamp',
        });

        const floatingPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [131, 64],
            extrapolate: 'clamp',
        });

        const backSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 25],
            extrapolate: 'clamp',
        });

        const backPosition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [65, -10],
            extrapolate: 'clamp',
        });
        const Titleposition = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [15, -50],
            extrapolate: 'clamp',
        });
        const TitleSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [20, 18],
            extrapolate: 'clamp',
        });
        const TitleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [35, 30],
            extrapolate: 'clamp',
        });
        const TripcodeMargin = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [5, -18],
            extrapolate: 'clamp',
        });
        const TripcodeFontSize = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [14, 10],
            extrapolate: 'clamp',
        });
        const TagCapsuleHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [32, 28],
            extrapolate: 'clamp',
        });
        const TagCapsulePadding = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [10, 8],
            extrapolate: 'clamp',
        });
        const TagCapsuleFOntSIze = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [12, 10],
            extrapolate: 'clamp',
        });
        return(
            <View style={styles.fill}>
                <TagRow tags={this.props.selected.trip!=undefined ? this.props.selected.trip.tags:[]}
                 floatingPosition={floatingPosition}
                 TagCapsuleHeight={TagCapsuleHeight}
                 TagCapsulePadding={TagCapsulePadding}
                 TagCapsuleFOntSIze={TagCapsuleFOntSIze}
                 />
                 {this.props.selected.trip!=undefined && this.props.selected.trip.has_joined!=true &&<RespondRow JoinThisTrip={this.JoinThisTrip.bind(this)} RejectThisTrip={this.RejectThisTrip.bind(this)}  /> }
                 {this.props.selected.trip!=undefined && this.props.selected.trip.has_joined==true &&<RespondRowBook id={this.props.selected.trip.id} /> }
                <ScrollView
                    style={styles.fill}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])
                    }>
                    <TripImage ip={this.props.ipadress.ipAdress} poster={this.props.selected.trip!=undefined ? this.props.selected.trip.images[0]:'-'}/>
                    <TripBody ip={this.props.ipadress.ipAdress} trip={this.props.selected.trip} />
                </ScrollView>
                <CustomHeader1 headerHeight={headerHeight}
                    backPosition={backPosition}
                    backSize={backSize}
                    HeaderTitle={this.props.selected.trip!= undefined ? this.props.selected.trip.name:'-'}
                    Titleposition={Titleposition}
                    TitleSize={TitleSize}
                    TitleHeight={TitleHeight}
                    TripcodeMargin={TripcodeMargin}
                    TripcodeFontSize={TripcodeFontSize}
                    tripcode={this.props.selected.trip != undefined ? this.props.selected.trip.trip_code : '-'}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fff'
    }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(TripViewInvited)