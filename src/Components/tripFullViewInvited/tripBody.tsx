import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

class TripBody extends React.Component {
    movetoItenerary(id, e) {
        let ids = id;
        Actions.itnerary_timeline({ id: ids, ip: this.props.ip })
    }
    movetoInvitees(id, e) {
        let ids = id;
        Actions.invitees({ id: ids, nameOfInvitee: this.props.trip.invited_by, nameOfTrip: this.props.trip.name });
    }
    render() {
        const { container, tripRow, rupeeSymbol, tripCost, responce,
            Invitee } = style;
        let packageId = this.props.trip != undefined ? this.props.trip.id : '-';
        return (
            <View style={container}>
                <View style={tripRow}>
                    <View style={{ width: '50%', flexDirection: 'row' }}>
                        <Image style={rupeeSymbol} source={require('../resourses/img/rupee.jpg')} />
                        <Text style={tripCost}>{this.props.trip != undefined ? this.props.trip.amount : '-'}</Text><Text style={{ color: '#767676', fontSize: 12 }}>/ Adult</Text>
                    </View>
                    <View style={{ alignItems: 'flex-end', width: '50%' }}>
                        {this.props.trip != undefined && (this.props.trip.has_joined == false && this.props.trip.has_rejected == false) && <Text style={[responce]}>Not Responded</Text>}
                        {this.props.trip != undefined && this.props.trip.has_joined == true && <Text style={[responce]}>JOINED</Text>}
                        {this.props.trip != undefined && this.props.trip.has_rejected == true && <Text style={[responce, { color: 'red' }]}>REJECTED</Text>}
                    </View>
                </View>
                <View style={[tripRow, { backgroundColor: '#efefef', paddingTop: 0, paddingBottom: 0 }]}>
                    <View style={{ width: '50%', flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#cccccc', paddingTop: 10, paddingBottom: 10 }}>
                        <Image style={Invitee} source={require('../resourses/img/profile.png')} />
                        <View style={{ padding: 5 }}>
                            <Text style={{ fontSize: 11, color: '#767676', marginBottom: 6 }}>Invited By</Text>
                            <Text style={{ fontSize: 13, fontWeight: '800', fontFamily: 'Merriweather-Bold' }}>{this.props.trip != undefined ? this.props.trip.invited_by : '-'}</Text>
                        </View>
                    </View>
                    <View style={{ width: '50%', flexDirection: 'row', paddingTop: 10, paddingBottom: 10, paddingLeft: 20 }}>
                        <Image style={[Invitee,{height:40,width:40,marginTop:2}]} source={require('../resourses/img/calendar.png')} />
                        <View style={{ padding: 5 }}>
                            <Text style={{ fontSize: 11, color: '#767676', marginBottom: 6 }}>Respond By</Text>
                            <Text style={{ fontSize: 13, fontWeight: '800', fontFamily: 'Merriweather-Bold' }}>18 Jun</Text>
                        </View>
                    </View>
                </View>

                <TouchableOpacity onPress={this.movetoItenerary.bind(this, packageId)} style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }]}>
                    <Image style={{ height: 40, width: 40, marginRight: 10 }} source={require('../resourses/img/itinerary.png')} />
                    <View style={{ width: '100%', alignItems: 'flex-start' }} ><Text style={{ fontSize: 13, textAlign: 'center' }}>Itinerary</Text></View>
                </TouchableOpacity>
                <TouchableOpacity  onPress={this.movetoInvitees.bind(this, packageId)} style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }]}>
                    <Image style={{ height: 40, width: 40, marginRight: 10 }} source={require('../resourses/img/travelers_invitees.png')} />
                    <View style={{ width: '100%', alignItems: 'flex-start' }}><Text style={{ fontSize: 13, textAlign: 'center' }}>Invitees</Text></View>
                </TouchableOpacity>
                <TouchableOpacity style={[tripRow, { alignItems: 'center', justifyContent: 'flex-start', marginBottom: 60, flexDirection: 'row' }]}>
                    <Image style={{ height: 40, width: 40, marginRight: 10 }} source={require('../resourses/img/more_info.png')} />
                    <Text style={{ fontSize: 13, textAlign: 'center' }}>Detailed Information</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const style = StyleSheet.create({
    container: {
        flex: 1
    },
    tripRow: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        padding: 20,
        flexDirection: 'row'
    },
    rupeeSymbol: {
        height: 15,
        width: 15,
        opacity: 0.7
    },
    tripCost: {
        fontWeight: '800',
        fontSize: 18,
        color: '#767676',
        alignSelf: 'flex-start',
        marginTop: -6,
        fontFamily: 'Merriweather-Bold'
    },
    responce: {
        fontSize: 12,
        color: '#767676'
    },
    Invitee: {
        height: 30,
        width: 30,
        borderRadius: 15,
        marginTop: 7
    }
})
export default TripBody;