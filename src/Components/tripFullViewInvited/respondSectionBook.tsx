import React from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

class RespondRowBook extends React.Component {
    movetoBooking(){
        Actions.booking({id:this.props.id});
    }
    render() {
        return (
            <View style={{
                flex: 1, position: 'absolute', bottom: 0, left: 0,
                right: 0, height: 60, zIndex: 1, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#ccc', shadowColor: '#ccc',
                shadowOffset: {
                    width: 0,
                    height: 0,
                },
                shadowRadius: 3,
                shadowOpacity: 0.3,
            }}><TouchableOpacity  style={{ height: 60, width: '100%', padding: 20, backgroundColor: '#FFAA5D', alignItems: 'center' }} onPress={this.movetoBooking.bind(this)}>
                <View >
                   <Text>Book</Text>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
}
export default RespondRowBook;