import React from 'react';
import { View, Text,TouchableOpacity } from 'react-native';

class RespondRow extends React.Component {
    render() {
        return (
            <View style={{
                flex: 1, position: 'absolute', bottom: 0, left: 0,
                right: 0, height: 60, zIndex: 1, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#ccc', shadowColor: '#ccc',
                shadowOffset: {
                    width: 0,
                    height: 0,
                },
                shadowRadius: 3,
                shadowOpacity: 0.3,
            }}><TouchableOpacity style={{ height: 60, width: '50%', padding: 20, backgroundColor: '#FFAA5D', alignItems: 'center' }} onPress={this.props.JoinThisTrip}>
                <View >
                    <Text>Join this Trip</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: 60, width: '50%', padding: 20, backgroundColor: '#fff', alignItems: 'center' }} onPress={this.props.RejectThisTrip}>
                <View >
                     <Text>No, Not Now</Text>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
}
export default RespondRow