import React from 'react';
import {View, Text} from 'react-native';

class SubHead extends React.Component{
    render(){
        return(
            <View style={{flex:1,height:40,justifyContent:'center',paddingLeft:30}}>
                <Text style={{fontWeight:'600',textAlign:'left'}}>Select Rooms</Text>
            </View>
        )
    }
}
export default SubHead;