import React from 'react';
import {View,Text,TouchableOpacity, Modal,StyleSheet} from 'react-native';
import { Icon } from 'react-native-material-ui';

export default class ChildAge extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            showPop:false,
            data:{},
        };
    }
    openSelector(data){
        // this.setState({showPop:true});
        this.props.openPopupChildAge(true)
        this.setState({data:data});
    }
    closeModal(){
        this.props.openPopupChildAge(false)
    }
    render(){
        return(
            <View style={{flex:1}}>
                {this.props.data.kids.map((item:any,i:any)=>{
                    console.log(item,"kids itemmssssss")
                        return(
                            <View key={i} style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginBottom:10}}>
                                <Text>Age of Kid {i+1}</Text>
                                
                                <TouchableOpacity onPress={this.openSelector.bind(this,item)}><Text style={{width:80,height:35,borderRadius:10,
                                    backgroundColor:'#fff',textAlign:'center',paddingTop:8,
                                    marginLeft:20,overflow:'hidden'}}>
                                {item.age}
                                </Text></TouchableOpacity>
                                
                            </View>
                        )
                    })}
                    <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.props.openStatus}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <View style={styles.containerStyle}>
                        <View style={styles.CardSectionStyle}>
                            <TouchableOpacity onPress={this.closeModal.bind(this)}>
                                <Icon size={20} name='close' style={{ alignSelf: 'flex-end', marginTop: 5, marginRight: 5 }} />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 15, marginBottom: 10, marginTop: 10 }}>select age</Text>
                            <View style={{flex:1}}> 
                            {Array.from(Array(10), (e, j) => {
                                return <TouchableOpacity onPress={this.props.setAgeKid.bind(this,this.state.data,(j+1))} key={j} style={{borderBottomWidth:1,borderBottomColor:'#efefef',padding:5,height:30}}>
                                            <Text style={{flex:1,fontSize:12}}>{j+1}</Text>
                                        </TouchableOpacity>
                            })}
                                
                            </View>
                        </View>
                    </View>
                    </Modal>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    CardSectionStyle: {
        borderBottomWidth: 1,
        padding: 10,
        flex:1,
        backgroundColor: '#fff',
        borderColor: '#ccc',
        position: 'relative',
        borderWidth: 1,
        shadowColor: '#767676',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        flexDirection: 'column',
        paddingBottom: 20,
    },
    containerStyle: {
        backgroundColor: 'rgba(255,255,255,0.75)',
        position: 'relative',
        flex: 1,
        padding: 10,
        justifyContent: 'center',
    }
});
