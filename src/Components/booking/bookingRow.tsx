import React from 'react';
import {View,Text} from 'react-native';

class BookingDefault extends React.Component{
    render(){
        return(
            <View style={{flex:1,backgroundColor:'#fff',padding:20,borderBottomWidth:1,borderBottomColor:'#ccc'}}>
                <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
                    <View style={{width:'40%',padding:10}}>
                            <Text style={{color:'#767676'}}>Type of room</Text>
                    </View>
                    <View style={{width:'60%',padding:10}}>
                            <Text style={{fontWeight:'600'}}>4 Star</Text>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
                    <View style={{width:'40%',padding:10}}>
                            <Text style={{color:'#767676'}}>No. of nights</Text>
                    </View>
                    <View style={{width:'60%',padding:10}}>
                            <Text style={{fontWeight:'600'}}>5</Text>
                    </View>
                </View>
            </View>
        )
    }
}
export default BookingDefault;