import React from 'react';
import {View, Text,TouchableOpacity} from 'react-native';
import RoomIndividual from './roomIndividual';


class Room extends React.Component{
    constructor(){
        super();
        this.state={
            RoomsCount:[{   id:1,
                            name:'room 1',
                            adults:0,
                            kids:[]
                        }],
        }
    }
    addKid(id){
        let Temp = [];
        let RoomsCount = this.state.RoomsCount;
        RoomsCount.map((data)=>{
            console.log(data,"data")
            if(data.id==id){
                console.log("id is equal")
                let temporary = {id:data.kids.length+1,age:1};
                console.log(temporary)
                data.kids.push(temporary);
                console.log(data)
                Temp.push(data);
            }else{
                Temp.push(data);
            }
        })
        this.setState({RoomsCount:Temp});
        this.props.calculateTotalCost(Temp);

    }
    removeKid(id){
        let Temp = [];
        let RoomsCount = this.state.RoomsCount;
        RoomsCount.map((data)=>{
            console.log(data,"row item")
            if(data.id==id){
                console.log("equla item")
                if(data.kids.length>0){
                    console.log("removed item")
                    let temporary = data.kids;
                    var index = 1;
                    console.log(temporary)
                    temporary.splice(0, 1);
                    console.log(temporary)
                    Temp.push(data);
                }else{
                    Temp.push(data);
                }
            }else{
                console.log("else item")
                Temp.push(data);
            }
        })
        this.setState({RoomsCount:Temp});
        this.props.calculateTotalCost(Temp);

    }
    addRomms(){
        let RoomsCount = this.state.RoomsCount;
        RoomsCount.push({id:RoomsCount.length+1,name:'room '+RoomsCount.length+1,
            adults:0,
            kids:[]
        })
        this.setState({RoomsCount})
        console.log(RoomsCount);
        this.props.calculateTotalCost(RoomsCount);

    }
    removeAdults(id){
        let Temp = [];
        let RoomsCount = this.state.RoomsCount;
        RoomsCount.map((data)=>{
            console.log(data,"row item")
            if(data.id==id){
                console.log("equla item")
                if(data.adults>0){
                    console.log("removed item")
                    data.adults=data.adults-1;
                    Temp.push(data);
                }else{
                    Temp.push(data);
                }
            }else{
                console.log("else item")
                Temp.push(data);
            }
        })
        this.setState({RoomsCount:Temp});
        this.props.calculateTotalCost(Temp);

    }
    addAdults(id){
        let Temp = [];
        let RoomsCount = this.state.RoomsCount;
        RoomsCount.map((data)=>{
            if(data.id==id){
                data.adults=data.adults+1;
                Temp.push(data);
            }else{
                Temp.push(data);
            }
        })
        this.setState({RoomsCount:Temp});
        this.props.calculateTotalCost(Temp);

    }
    setChildAGe(childInfo:any){
        console.log(childInfo,"sending data")
        let Temp = [];
        let RoomsCount = this.state.RoomsCount;

        RoomsCount.map((data)=>{
            console.log(data,"master data")
            if(data.id==childInfo.id){
                console.log(data.id,"parent node finding",childInfo.id)
                data.kids.map((kid,k)=>{
                    console.log(kid)
                    console.log(kid.id,"&&&&&&&&&&",childInfo.child.id)
                    if(kid.id==childInfo.child.id){
                        console.log("got the child",kid)
                        kid['age'] = childInfo.age;
                    }
                })
                console.log(data)
                Temp.push(data);
                // console.log("id is equal")
                // let temporary = {id:data.kids.length+1,age:0};
                // console.log(temporary)
                // data.kids.push(temporary);
                // console.log(data)
                // Temp.push(data);
            }else{
                Temp.push(data);
            }
        })
        this.setState({RoomsCount:Temp});
        this.props.calculateTotalCost(Temp);

        console.log(Temp,"got thedata")
    }
    render(){
        console.log(this.state.RoomsCount)
        return(
            <View style={{flex:1,padding:10,marginBottom:50}}>
                {this.state.RoomsCount.map((item:any,i:any)=>{
                     return (
                     <View style={{flex:1}} key={i}>
                        <View style={{flex:1,height:30,width:'100%'}}>
                            <Text style={{paddingLeft:20,marginBottom:10,alignSelf:'flex-start',position:'absolute'}}>Room {i+1}</Text>
                                {i==0 && <Text style={{marginBottom:10,fontSize:12,alignSelf:'flex-end',color:'#767676'}}>Select occupants to see the cost</Text>
                        }
                            </View>
                        <RoomIndividual setChildAGe={this.setChildAGe.bind(this)} removeKid={this.removeKid.bind(this)} addKid={this.addKid.bind(this)}  addAdults={this.addAdults.bind(this)} removeAdults={this.removeAdults.bind(this)} key={i} data={item} />
                     </View>)
                })}
                   
            <View style={{flex:1,padding:20,alignItems:'flex-end'}}>
               <TouchableOpacity onPress={this.addRomms.bind(this)}>
                    <View style={{width:100,borderWidth:1,borderColor:'#FF6227',height:30,justifyContent:'center',alignItems:'center',borderRadius:10}}> <Text style={{color:'#FF6227',fontWeight:'600'}}>Add Rooms</Text></View>
               </TouchableOpacity>
            </View>
            </View>
        )
    }
}
export default Room;