import React from 'react';
import {View,Text, TouchableOpacity, Picker} from 'react-native';
import ChildAge from './childAge';

class RoomIndividual extends React.Component{
    constructor(){
        super();
        this.state = {
            open:false,
        }
    }
    setAgeKid(data,age,e){
        console.log(data,"data from ",age,e)
        let temp ={
            id:this.props.data.id,
            child:data,
            age:age
        }
        this.props.setChildAGe(temp);
        this.setState({open:false});
    }
    openPopupChildAge(data){
        this.setState({open:data});
    }
    render(){
        let id = this.props.data.id;
        return(
            <View style={{flex:1,backgroundColor:'#efefef',borderRadius:7,padding:20,marginBottom:20}}>
                <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:'50%',justifyContent:'center',alignItems:'center',height:35}}>
                        <Text style={{color:'#767676'}}>No of Adults</Text>
                    </View>
                    <View style={{width:'50%',height:35,flexDirection:'row',paddingTop:5}}>
                        <TouchableOpacity onPress={this.props.removeAdults.bind(this,id)}>
                            <Text style={{paddingLeft:5,overflow:'hidden',width:20,height:20,borderWidth:1,borderRadius:10,borderColor:'#ccc',backgroundColor:(this.props.data.adults > 0 ? '#FD8542':'#fff') }}>-</Text>
                        </TouchableOpacity>
                            <Text style={{marginLeft:20,marginRight:20}}>{this.props.data.adults}</Text>
                        <TouchableOpacity onPress={this.props.addAdults.bind(this,id)}>
                            <Text style={{paddingLeft:5,overflow:'hidden',width:20,height:20,borderWidth:1,borderRadius:10,borderColor:'#ccc',backgroundColor:'#FD8542' }}>+</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row'}}>
                    <View style={{width:'50%',justifyContent:'center',alignItems:'center',height:35}}>
                        <Text  style={{color:'#767676'}}>No of Kids</Text>
                    </View>
                    <View style={{width:'50%',height:35,flexDirection:'row',paddingTop:5}}>
                        <TouchableOpacity onPress={this.props.removeKid.bind(this,id)}>
                            <Text style={{paddingLeft:5,overflow:'hidden',width:20,height:20,borderWidth:1,borderRadius:10,borderColor:'#ccc',backgroundColor:(this.props.data.kids.length > 0 ? '#FD8542':'#fff') }}>-</Text>
                        </TouchableOpacity>
                            <Text style={{marginLeft:20,marginRight:20}}>{this.props.data.kids.length}</Text>
                        <TouchableOpacity onPress={this.props.addKid.bind(this,id)}>
                            <Text style={{paddingLeft:5,overflow:'hidden',width:20,height:20,borderWidth:1,borderRadius:10,borderColor:'#ccc',backgroundColor:'#FD8542' }}>+</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex:1}}>
                    <ChildAge openStatus={this.state.open} openPopupChildAge={this.openPopupChildAge.bind(this)} setAgeKid={this.setAgeKid.bind(this)}  data={this.props.data} />
                </View>
                <Text style={{color:'#767676',alignSelf:'flex-end',fontSize:10,marginTop:10}}>Infants upto 2 years stay free of charge</Text>
             </View>
        )
    }
}
export default RoomIndividual;