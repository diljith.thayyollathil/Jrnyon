import React from 'react';
import {View,Text} from 'react-native';
class TripCostRow extends React.Component{
    render(){
        return(
            <View style={{backgroundColor:'#c6c6c6',height:35,justifyContent:'center',alignItems:'center'}}>
                <Text style={{textAlign:'center', fontSize:12}}>Trip Cost per person <Text style={{fontWeight:'800'}}>$ 90,000</Text></Text>
            </View>
        )
    }
}
export default TripCostRow;