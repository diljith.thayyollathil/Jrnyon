import React from 'react';
import {View, StyleSheet, Animated} from 'react-native';

import { TextField } from 'react-native-material-textfield';
import PhoneInput from 'react-native-phone-input'

class LoginFields extends React.Component{
    constructor(){
        super();
        this.state = {
            phone:'',
            fullname:''
        }
    }
    componentWillMount() {
        this.animatedValue = new Animated.Value(0);
    }
    componentDidMount(){
        Animated.timing(this.animatedValue, {
            toValue: 1,
            duration: 1500,
            delay: 1200
        }).start();
    }
    onPhoneChange(e:any){
        this.setState({ phone:e });
        this.props.FullnameandPhno(e,"phone");
    }
    render(){
        let animatedValue = this.animatedValue;
        let { phone, fullname } = this.state;
        let {loginfieldcontainer} = styles;
        return(
            <Animated.View style={[loginfieldcontainer,{opacity:animatedValue,marginBottom:this.props.margin}]}>
                <View style={{borderBottomWidth:1,borderBottomColor:'#c6c6c6',padding:10}}>
                <TextField
                    label='Full Name'
                    ref={this.props.usernameRef}
                    value={fullname}
                    onChangeText={ (fullname) => {this.setState({ fullname });
                    this.props.FullnameandPhno(fullname,"fullname");
                }}
                    lineWidth={0}
                    baseColor={"#333"}
                    tintColor={"#757575"}
                    activeLineWidth={0}
                    onFocus={this.props.onFocus}
                    labelHeight={25}
                    fontSize={16}
                    underlineColorAndroid="transparent"
                    error={this.props.errors.fullname}
                />
                </View>
                <View style={{padding:10,paddingTop:30,paddingBottom:20}}>
                <PhoneInput initialCountry='in' ref={this.props.phonenumberRef} returnKeyType='done' onChangePhoneNumber={this.onPhoneChange.bind(this)}/>
                </View>
            </Animated.View>
        )
    }
}
const styles = StyleSheet.create({
    loginfieldcontainer:{
        zIndex:2,
        borderWidth:1,
        borderColor:'#efefef',
        backgroundColor:'#fff',
        position:'absolute',
        left:0,
        right:0,
        bottom:60,
        paddingLeft:20,
        paddingRight:20
    }
})
export default LoginFields;