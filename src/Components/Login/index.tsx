import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Action from '../../actions';
import { View, Text, Image, StyleSheet, Dimensions,
Animated, TouchableOpacity, AsyncStorage, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Splash from './login_starting';
import Logo from './Logo';
import LoginFields from './LoginFields';
import OtpField from './otpField';


class Login extends React.Component {
    constructor(){
        super();
        this.state={
            errors:{},
            submit:'SIGN IN',
            backButton:false,
            user: null,
            message: '',
            codeInput: '',
            phoneNumber: '+919895377662',
            fullname:'',
            mobile_no:'',
            confirmResult: null,
        }
        this.usernameRef = this.updateRef.bind(this, 'username');
        this.phonenumberRef = this.updateRef.bind(this, 'phonenumber');
    }
    updateRef(name, ref) {
        this[name] = ref;
    }
    onFocus() {
        let { errors = {} } = this.state;
        for (let name in errors) {
          let ref = this[name];

          if (ref && ref.isFocused()) {
            delete errors[name];
          }
        }
        this.setState({ errors });
      }

    componentWillMount() {

        const win = Dimensions.get('window');
        this.animatedValue = new Animated.Value(win.height);
        this.animatedMargin = new Animated.Value(0);
        this.animatedOtp = new Animated.Value(-200);
    }
    reverseAction(){
        this.setState({backButton:false});
        this.setState({submit:'SIGN IN'});

        // Animating the login fields after clicking back
        const win = Dimensions.get('window');
        Animated.parallel([
            Animated.timing(this.animatedMargin, {
                toValue: 0,
                duration: 800,
                delay: 100
            }),
            Animated.timing(this.animatedOtp, {
                toValue: -200,
                duration: 800,
                delay: 100
            })
        ]).start();
    }
    async sendOTPandAnimate(){
        let regx = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
        let errors = {};
        let error = false;
        if(this.state.fullname==''){
            error = true;
            errors['fullname'] = 'should not be empty';
        }
        if(this.state.mobile_no==''){
            error = true;
            errors['mobile_no'] = 'Should not be empty';
        }else{
            if(!regx.test(this.state.mobile_no)){
                error = true;
                errors['mobile_no'] = 'Phone number not valid';
            }
        }
        console.log(errors,"error occured");
        await this.setState({ errors });

        if( error === false  ){
            let state =  this.state;
            state['form'] ='phone_no_form';
            // Fetch method for submitting phno and fullname
            await this.props.actions.trylogin(state);
           
        }
    }




    componentDidUpdate(){
         if(this.props.Login.loginAttempt!=undefined){
                if(this.props.Login.loginAttempt==true){
                    this.props.actions.resetLoginAttempt()
                    this.setState({backButton:true});
                    this.setState({submit:'SUBMIT'});        
                    // Animating the login fields after clicking submit
                        const win = Dimensions.get('window');
                        Animated.parallel([
                            Animated.timing(this.animatedValue, {
                                toValue: (win.height - 80),
                                duration: 800,
                                delay: 100
                            }),
                            Animated.timing(this.animatedMargin, {
                                toValue: -300,
                                duration: 800,
                                delay: 100
                            }),
                            Animated.timing(this.animatedOtp, {
                                toValue: 0,
                                duration: 800,
                                delay: 100
                            })
                        ]).start()
                    }
            }

    }
    movetoHomeScreen(){
        let state = this.state;
        state['form'] = 'verify_otp_form';

        // otp Verification

        this.props.actions.verifyOtp(state)
    }
    // Parent function which passed to fields for collecting phno and fullname
    async FullnameandPhno(data,type){
        if(type==='fullname'){
            await this.setState({fullname:data})
        }else{
            let mobile_no = data;
            mobile_no = mobile_no.replace(/ /g,'');
            await this.setState({mobile_no:mobile_no});
        }
    }
    OtpValue(data){
        this.setState({otp:data});
    }
    render() {
        let {LoginContainer , Backgroundimage} = styles;
        let animatedValue = this.animatedValue;
        let animatedMargin = this.animatedMargin;
        let animatedOtp = this.animatedOtp;
        return (
           
            <View style={LoginContainer}>
                 <TouchableOpacity style={LoginContainer} onPress={()=>{Keyboard.dismiss()}} >
                    {this.state.backButton===true ? <TouchableOpacity onPress={this.reverseAction.bind(this)} style={{position:'absolute',top:30,left:16,zIndex:999}}>
                        <Image source={require('../resourses/img/back.png')} style={{height:40,width:40}} />
                    </TouchableOpacity>:<Text></Text>}
                    <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss()}} >
                        <Animated.Image style={[Backgroundimage,{height:animatedValue}]} source={require('../resourses/img/background.png')}/>
                    </TouchableWithoutFeedback>
                    <Splash submit={this.state.submit} sendOTPandAnimate={this.sendOTPandAnimate.bind(this)} movetoHomeScreen={this.movetoHomeScreen.bind(this)}/>
                    <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss()}} > 
                        <Logo/>
                    </TouchableWithoutFeedback>
                    <LoginFields  onFocus={this.onFocus.bind(this)} errors={this.state.errors} usernameRef={this.usernameRef} phonenumberRef={this.phonenumberRef} margin={animatedMargin} FullnameandPhno={this.FullnameandPhno.bind(this)}/>
                    <OtpField phonenumber={this.state.mobile_no} OtpValue={this.OtpValue.bind(this)} sendOTPandAnimate={this.sendOTPandAnimate.bind(this)} animatedOtp={animatedOtp} />
                </TouchableOpacity>
            </View>
           
        )
    }
}

const win = Dimensions.get('window');

const styles =StyleSheet.create( {
    LoginContainer:{
        flex:1,
        backgroundColor:'#fff'
    },
    Backgroundimage:{
        flex: 1,
        width:win.width,
        position:'absolute',
        left:0,
        right:0,
        top:0,
        bottom:0
    }
});
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = (dispatch: any) => ({ actions: bindActionCreators(Action, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Login);
