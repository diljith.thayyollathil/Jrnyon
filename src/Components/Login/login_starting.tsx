import React from 'react';
import { View, Animated, Easing, Dimensions, Text, TouchableOpacity , Platform} from 'react-native';

import { Actions } from 'react-native-router-flux';

class Splash extends React.Component {
    constructor(){
        super();
        this.state={
        }
    }
    componentWillMount() {
        this.animatedValue = new Animated.Value(0);
        this.animatedOpacity = new Animated.Value(.5);
        this.textOpacity = new Animated.Value(0);
    }
    componentDidMount() {
        const window = Dimensions.get('window');
        let heightforsplash = Platform.OS === 'ios'? 60:83;
        Animated.parallel([
            Animated.timing(this.animatedValue, {
                toValue: (window.height - heightforsplash),
                duration: 1000,
                delay: 1000
            }),
            Animated.timing(this.animatedOpacity, {
                toValue: 1,
                duration: 1000,
                delay: 1000
            }),
            Animated.timing(this.textOpacity, {
                toValue: 1,
                duration: 1000,
                delay: 1000
            })
        ]).start();
    }
    render() {
        let animatedStyle = this.animatedValue;
        let textOpacity = this.textOpacity;
        let animatedOpacity = this.animatedOpacity;
        let { SplashContainer, signintext } = styles;
        return (

            <Animated.View style={[SplashContainer, { top: animatedStyle, opacity:animatedOpacity }]}>
                <TouchableOpacity onPress={()=>{

                        if(this.props.submit==='SUBMIT'){
                            this.props.movetoHomeScreen()
                        }else{
                            this.props.sendOTPandAnimate();
                        }
                    }}>
                    <Animated.Text style={[signintext, { opacity: textOpacity }]}>{this.props.submit} </Animated.Text>
                </TouchableOpacity>
            </Animated.View>

        )
    }
}
const styles = {
    SplashContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 1,
        backgroundColor: '#000'
    },
    signintext: {
        color: '#fff',
        fontSize:16,
        fontWeight:'600',
        alignSelf: 'center',
        marginTop: 18
    }
}

export default Splash;