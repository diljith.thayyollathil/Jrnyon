import React from 'react';
import {View, Animated,Text,TouchableOpacity} from 'react-native';
import { TextField } from 'react-native-material-textfield';

class OtpField extends React.Component{
    constructor(){
        super();
        this.state={
            otp:''
        }
    }
    render(){
        return(
            <Animated.View style={{position:'absolute',
            left:0,right:0,bottom:60,zIndex:0,marginBottom:this.props.animatedOtp}}>
                <View style={{backgroundColor:'rgba(0,0,0,0.5)',flex:1,height:35,padding:10}}>
                    <Text style={{alignSelf:'flex-start',color:'#fff',fontSize:11, position:'absolute',left:10,top:10}}> 
                        OTP sent to {this.props.phonenumber}
                    </Text>
                    <TouchableOpacity onPress={this.props.sendOTPandAnimate} style={{alignSelf:'flex-end'}}>
                        <Text style={{color:'#fff'}}>RESEND OTP</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1,backgroundColor:'#fff',padding:10,paddingTop:0,paddingLeft:20,paddingRight:20}}>
                    <TextField
                        label='Enter OTP Code'
                        value={this.state.otp}
                        onChangeText={ (otp) => {this.setState({ otp });
                        this.props.OtpValue(otp);
                        }
                        }
                        lineWidth={0}
                        labelHeight={25}
                        activeLineWidth={0}
                        fontSize={16}
                        baseColor={"#333"}
                        tintColor={"#757575"}
                    />
                </View>
            </Animated.View>
        )
    }
}
export default OtpField;