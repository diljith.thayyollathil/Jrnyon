import React from 'react';
import {View,Image, StyleSheet,Animated} from 'react-native';

class Logo extends React.Component{
    componentWillMount(){
        this.animatedValue = new Animated.Value(300);
        this.animatedSize = new Animated.Value(80);
    }
    componentDidMount(){
        Animated.parallel([
            Animated.timing(this.animatedValue, {
                toValue: 180,
                duration:1000,
                delay:1000
            }),
            Animated.timing(this.animatedSize, {
                toValue: 140,
                duration:1000,
                delay:1000
            })
        ]).start()
    }
    render(){
        let animatedStyle = this.animatedValue;
        let animatedSize = this.animatedSize;
        const {AppLogo} = styles;
        return(
            <View style={{zIndex:1}}>
                <Animated.Image style={[AppLogo,{marginTop:animatedStyle,height:animatedSize}]} source={require('../resourses/img/logo.png')} />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    AppLogo:{
        width:150,
        height:150,
        alignSelf:'center',
        resizeMode:'contain'
    }
})
export default Logo;