import {
    LOGIN_DETAILS, FETCH_ITENERARIES, FETCH_INVITEES, FETCH_MYTRIP,
    FETCH_ITENERIES_FAILURE, FETCH_ITENERIES_BEGIN, FETCH_ALLTRIP_BEGIN,
    FETCH_ALLTRIP_SUCCESS, FETCH_ALLTRIP_FAILURE,
    FETCH_SINGLETRIP_SUCCESS, FETCH_SINGLETRIP_BEGIN, FETCH_SINGLETRIP_FAILURE,
    FETCH_MYTRIP_BEGIN, FETCH_MYTRIP_FAILURE, FETCH_MYTRIP_SUCCESS,
    ENQUIRE_BEGIN, ENQUIRE_FAILURE, ENQUIRE_SUCCESS, ADD_TO_FAVOURITES_BEGIN,
    ADD_TO_FAVOURITES_SUCCESS, ADD_TO_FAVOURITES_FAILURE, FETCH_ENQUIRIES_BEGIN,
    FETCH_ENQUIRIES_FAILURE, FETCH_ENQUIRIES_SUCCESS, FETCH_FAVOURITES_BEGIN,
    FETCH_FAVOURITES_SUCCESS, FETCH_FAVOURITES_FAILURE, ADD_INVITEES, REMOVE_ENQUIRED,
    ADD_TO_ENQUIRE_LIST, ADD_TO_FAVOURITE_STORE, LOGIN_START, OTP_VERIFY, FETCH_INVITEES_BEGIN,
    FETCH_INVITEES_FAILURE, FETCH_INVITEES_SUCCESS, ADDINVITEES_FAILURE, ADD_TO_INVITEE_BEGIN,
    REMOVE_FROM_FAVOURITES, LOGIN_FAILURE, LOGIN_SUCCESS, RESET_LOGIN_ATTEMPT,
    JOIN_TRIP_SUCCESS, JOIN_TRIP_FAILURE, JOIN_TRIP_BEGIN, FETCH_ALL_UPCOMING_BEGIN, FETCH_ALL_UPCOMING_FAILURE,
    FETCH_ALL_UPCOMING_SUCCESS, FETCH_CURENT_TRIP_BEGIN, FETCH_CURENT_TRIP_SUCCESS, FETCH_CURENT_TRIP_FAILURE,
    UPLOAD_TO_GALLERY_SUCCESS, UPLOAD_TO_GALLERY_BEGIN, UPLOAD_TO_GALLERY_FAILURE, FETCH_SINGLE_UPCOMING_BEGIN,
    FETCH_SINGLE_UPCOMING_SUCCESS, FETCH_SINGLE_UPCOMING_FAILURE, FETCH_GALLERY_BEGIN, FETCH_GALLERY_FAILURE, FETCH_GALLERY_SUCCESS,
    GALLERY_INDIVIDUAL_BEGIN, GALLERY_INDIVIDUAL_FAILURE, GALLERY_INDIVIDUAL_SUCCESS,
    REJECT_TRIP_BEGIN, REJECT_TRIP_FAILURE, REJECT_TRIP_SUCCESS, SET_GALLERY_ID, FETCH_GALLERY_ID, SET_IP_ADDRESS,
    FETCH_BOOKING_INFO_BEGIN, FETCH_BOOKING_INFO_SUCCESS, FETCH_BOOKING_INFO_FAILURE
} from './types';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

const localIP = "http://10.191.2.219:8083";
const serverIP = "http://13.126.141.49";


const Ip = serverIP;

let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjozNiwidGltZSI6IjA4QXVndXN0MjAxODExMDQ0NSJ9.18X2P4pk8Ofyhx_viDHUFACroWQ-wuP75WXNS7hPbjA";
export const setIpAddress = () => {
    return {
        type:SET_IP_ADDRESS,
        data:Ip
    }
}
export const login_start = () => {
    return {
        type: LOGIN_START,
    }
}
export const otp_verify_start = () => {
    return {
        type: OTP_VERIFY,
    }
}
export const verifyOtp = (data) => {
    return dispatch => {
        dispatch(otp_verify_start())
        fetch( Ip + '/account/login/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data),
        }).then(handleErrors)
            .then((response) => response.json())
            .then((responseJson) => {
                token = responseJson.token;
                console.log(JSON.stringify(responseJson), "yessss")
                AsyncStorage.setItem('login', JSON.stringify(responseJson));
                console.log(responseJson, "response");
                Actions.home();
            })
            .catch((error) => {
                console.log("error occured")
            });
    }
}


export const trylogin = (data) => {
    return dispatch => {
        dispatch(login_start());

        fetch(Ip + '/account/login/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data),
        }).then((response) => {
            const statusCode = response.status;
            const data = response.json();
            return Promise.all([statusCode, data]);
        })
            .then(([statusCode, data]) => {
                if (statusCode != 200) {
                    Actions.errorPage({ message: data.message });
                    dispatch(login_failure())
                } else {
                    dispatch(login_success())
                }
            })
            .catch((error) => {
                console.log("error occured")
                console.error(error);
            });

    }
}

export const login_failure = () => {
    return {
        type: LOGIN_FAILURE,
    }
}
export const login_success = () => {
    return {
        type: LOGIN_SUCCESS,
    }
}
export const resetLoginAttempt = () => {
    return {
        type: RESET_LOGIN_ATTEMPT
    }
}


function handleErrors(response) {
    console.log(response,"Backend Response")
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}


// *********** Invitees *****************

export const fetch_invitees = (data) => {
    console.log(data, "####################")
    return dispatch => {
        dispatch(fetch_invitees_begin());
        return fetch( Ip + "/api/schedule_package/" + data + "/invitees/", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_invitees_success(json));
                return json;
            })
            .catch(error => {
                dispatch(fetch_invitees_failure())
                console.log(error, "###########################")
            });
    };
}

export const fetch_invitees_begin = () => {
    return {
        type: FETCH_INVITEES_BEGIN,
    }
}
export const fetch_invitees_success = (data) => {
    return {
        type: FETCH_INVITEES_SUCCESS,
        data: data
    }
}
export const fetch_invitees_failure = () => {
    return {
        type: FETCH_INVITEES_FAILURE
    }
}

export const addInviteesToList = (data) => {
    return dispatch => {
        dispatch(add_to_invitees_begin());
        return fetch( Ip + "/api/schedule_package/" + data.id + "/invite/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
            body: JSON.stringify(data),
        }).then((response) => response.json())
            .then((responseJson) => {
                dispatch(inviteeAdded(data))
                console.log(responseJson, "response")
            })
            .catch((error) => {
                console.log(error, "inside add fetch")
                dispatch(inviteeAddingFaiure())
                console.error(error);
            });

    }
}

export const add_to_invitees_begin = () => {
    return {
        type: ADD_TO_INVITEE_BEGIN
    }
}
export const inviteeAddingFaiure = () => {
    return {
        type: ADDINVITEES_FAILURE
    }
}

export const inviteeAdded = (data) => {
    console.log(data, "inside action")
    return {
        type: ADD_INVITEES,
        data: data
    }
}
// ************* All Upcoming Trips *************
export const fetch_all_upcoming_trips = () => {
    console.log("Got the calll")
    return dispatch => {
        dispatch(fetch_all_upcoming_trips_begin());
        return fetch( Ip + "/api/schedule_package/upcoming/all", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_all_upcoming_trips_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_all_upcoming_trips_failure()));
    };
}
export const fetch_all_upcoming_trips_begin = () => {
    console.log("begin the call")
    return {
        type: FETCH_ALL_UPCOMING_BEGIN,
    }
}
export const fetch_all_upcoming_trips_failure = () => {
    console.log("failed the call")
    return {
        type: FETCH_ALL_UPCOMING_FAILURE,
    }
}
export const fetch_all_upcoming_trips_success = (data) => {
    console.log("success call")
    return {
        type: FETCH_ALL_UPCOMING_SUCCESS,
        data: data
    }
}
// ************* single Upcoming Trips *************
export const fetch_single_upcoming_trips = () => {
    return dispatch => {
        dispatch(fetch_single_upcoming_trips_begin());
        return fetch( Ip + "/api/schedule_package/upcoming/", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_single_upcoming_trips_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_single_upcoming_trips_failure()));
    };
}
export const fetch_single_upcoming_trips_begin = () => {
    return {
        type: FETCH_SINGLE_UPCOMING_BEGIN,
    }
}
export const fetch_single_upcoming_trips_failure = () => {
    return {
        type: FETCH_SINGLE_UPCOMING_FAILURE,
    }
}
export const fetch_single_upcoming_trips_success = (data) => {
    return {
        type: FETCH_SINGLE_UPCOMING_SUCCESS,
        data: data
    }
}

// ************* Current trip **********
export const fetch_current_trip = () => {
    return dispatch => {
        dispatch(fetch_current_trip_begin());
        return fetch(Ip + "/api/schedule_package/current", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_current_trip_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_current_trip_failure()));
    };
}
export const fetch_current_trip_begin = () => {
    return {
        type: FETCH_CURENT_TRIP_BEGIN,
    }
}
export const fetch_current_trip_failure = () => {
    return {
        type: FETCH_CURENT_TRIP_FAILURE,
    }
}
export const fetch_current_trip_success = (data) => {
    return {
        type: FETCH_CURENT_TRIP_SUCCESS,
        data: data
    }
}


// *********** My Trips **************
export const fetch_myTrips_invites = () => {
    return dispatch => {
        dispatch(fetch_my_trips_begin());
        return fetch(Ip + "/api/schedule_package/my_trips", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_my_trips_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_my_trips_failure()));
    };
}

export const fetch_my_trips_begin = () => {
    return {
        type: FETCH_MYTRIP_BEGIN,
    }
}
export const fetch_my_trips_failure = () => {
    return {
        type: FETCH_MYTRIP_FAILURE,
    }
}
export const fetch_my_trips_success = (data) => {
    return {
        type: FETCH_MYTRIP_SUCCESS,
        data: data
    }
}
//*********** iteneraries **********

export const fetch_itneraries = (id) => {
    return dispatch => {
        dispatch(fetch_iteneries_begin());
        return fetch( Ip + "/api/package/itineraries/" + id, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_itneraries_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_iteneries_failure()));
    };
}
export const fetch_itneraries_success = (data) => {
    return {
        type: FETCH_ITENERARIES,
        data: data
    }
}
export const fetch_iteneries_begin = () => {
    return {
        type: FETCH_ITENERIES_BEGIN,
    }
}
export const fetch_iteneries_failure = () => {
    return {
        type: FETCH_ITENERIES_FAILURE,
    }
}

// ********** all trips ************

export const fetchalltrips = () => {
    return dispatch => {
        dispatch(fetch_all_trips_begin());
        return fetch( Ip + "/api/schedule_package/?view=all", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_all_trips_success(json));
                return json;
            })
            .catch((error) => {
                console.log(error)
                dispatch(fetch_all_trips_failure())
            }
            );
    };
}

export const fetch_all_trips_begin = () => {
    return {
        type: FETCH_ALLTRIP_BEGIN,
    }
}
export const fetch_all_trips_failure = () => {
    return {
        type: FETCH_ALLTRIP_FAILURE,
    }
}
export const fetch_all_trips_success = (data) => {
    return {
        type: FETCH_ALLTRIP_SUCCESS,
        data: data
    }
}


// ********** enquiries ************

export const fetchallenquiries = () => {
    return dispatch => {
        dispatch(fetch_enquiries_begin());
        return fetch( Ip + "/api/schedule_package/?view=enquire", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_enquiries_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_enquiries_failure()));
    };
}

export const fetch_enquiries_begin = () => {
    return {
        type: FETCH_ENQUIRIES_BEGIN,
    }
}
export const fetch_enquiries_failure = () => {
    return {
        type: FETCH_ENQUIRIES_FAILURE,
    }
}
export const fetch_enquiries_success = (data) => {
    return {
        type: FETCH_ENQUIRIES_SUCCESS,
        data: data
    }
}

// ********** favourites ************

export const fetchallfavourites = () => {
    return dispatch => {
        dispatch(fetch_favourites_begin());
        return fetch( Ip + "/api/schedule_package/?view=favourite", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_favourites_success(json));
                return json;
            })
            .catch(error => dispatch(fetch_favourites_failure()));
    };
}

export const fetch_favourites_begin = () => {
    return {
        type: FETCH_FAVOURITES_BEGIN,
    }
}
export const fetch_favourites_failure = () => {
    return {
        type: FETCH_FAVOURITES_FAILURE,
    }
}
export const fetch_favourites_success = (data) => {
    return {
        type: FETCH_FAVOURITES_SUCCESS,
        data: data
    }
}

// *********** fetch single trip detail ************

export const fetchTripDetails = (id) => {
    return dispatch => {
        dispatch(fetch_single_trips_begin());
        return fetch( Ip + "/api/schedule_package/view/" + id, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_single_trips_success(json));
                return json;
            })
            .catch(error => {

                dispatch(fetch_single_trips_failure())
            });
    };
}

export const fetch_single_trips_begin = () => {
    return {
        type: FETCH_SINGLETRIP_BEGIN,
    }
}
export const fetch_single_trips_failure = () => {
    return {
        type: FETCH_SINGLETRIP_FAILURE,
    }
}
export const fetch_single_trips_success = (data) => {
    return {
        type: FETCH_SINGLETRIP_SUCCESS,
        data: data
    }
}

// ************** Enquire to team **************

export const enquire_to_team = (item) => {
    return dispatch => {
        dispatch(enquire_begin());
        return fetch( Ip + "/api/schedule_package/enquire/" + item.id, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            }
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(enquire_success(json));
                dispatch(remove_Enquired(item.id));
                dispatch(addToEnquires(item))
                return json;
            })
            .catch(error => {
                dispatch(enquire_failure())
            });
    };
}
export const addToEnquires = (item) => {
    return {
        type: ADD_TO_ENQUIRE_LIST,
        data: item
    }
}
export const enquire_begin = () => {
    return {
        type: ENQUIRE_BEGIN,
    }
}
export const remove_Enquired = (data) => {
    return {
        type: REMOVE_ENQUIRED,
        data: data
    }
}
export const enquire_failure = () => {
    return {
        type: ENQUIRE_FAILURE,
    }
}
export const enquire_success = (data) => {
    return {
        type: ENQUIRE_SUCCESS,
        data: data
    }
}


// *********** add to favourites **************

export const add_to_favourites = (item) => {

    return dispatch => {
        dispatch(add_to_favourites_begin());
        return fetch(Ip + "/api/schedule_package/favourite/" + item.data.id + "/?view=favourite", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
            body: JSON.stringify(item),
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(add_to_favourites_store(item.data))
                dispatch(add_to_favourites_success());
                return json;
            })
            .catch(error => {
                dispatch(add_to_favourites_failure())
            });
    };
}

export const add_to_favourites_store = (item) => {
    return {
        type: ADD_TO_FAVOURITE_STORE,
        data: item
    }
}

export const add_to_favourites_begin = () => {
    return {
        type: ADD_TO_FAVOURITES_BEGIN,
    }
}
export const add_to_favourites_failure = () => {
    return {
        type: ADD_TO_FAVOURITES_FAILURE,
    }
}
export const add_to_favourites_success = () => {
    return {
        type: ADD_TO_FAVOURITES_SUCCESS,
    }
}
export const remove_from_favourites = (data) => {
    return {
        type: REMOVE_FROM_FAVOURITES,
        data: data
    }
}



// ************** Join a trip invited **********

export const JoinTrip = (item: any) => {
    console.log("Join Trip started", item)
    return dispatch => {
        dispatch(join_trip_begin());
        return fetch( Ip + "/api/schedule_package/" + item + "/join/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
            body: JSON.stringify(item),
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(join_trip_success())
                dispatch(fetch_myTrips_invites())
                return json;
            })
            .catch(error => {
                dispatch(join_trip_failure())
            });
    }
}

export const join_trip_begin = () => {
    return {
        type: JOIN_TRIP_BEGIN
    }
}
export const join_trip_failure = () => {
    return {
        type: JOIN_TRIP_FAILURE
    }
}
export const join_trip_success = () => {
    return {
        type: JOIN_TRIP_SUCCESS
    }
}

// reject a trip invited

export const RejectTrip = (item: any) => {
    return dispatch => {
        dispatch(reject_trip_begin());
        return fetch( Ip + "/api/schedule_package/" + item + "/reject/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
            body: JSON.stringify(item),
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(reject_trip_success())
                dispatch(fetch_myTrips_invites())
                return json;
            })
            .catch(error => {
                dispatch(reject_trip_failure())
            });
    }
}

export const reject_trip_begin = () => {
    return {
        type: REJECT_TRIP_BEGIN
    }
}
export const reject_trip_failure = () => {
    return {
        type: REJECT_TRIP_FAILURE
    }
}
export const reject_trip_success = () => {
    return {
        type: REJECT_TRIP_SUCCESS
    }
}
// ************** set gallery ID ******************
export const setGalleryId = (data) =>{
    return {
        type:SET_GALLERY_ID,
        data:data
    }
}
export const fetchGalleryId =()=>{
    return{
        type:FETCH_GALLERY_ID
    }
}

// **************** upload to gallery *****************

export const upload_to_gallery = (item: any) => {
    console.log(item,"sending data")
    return dispatch => {
        dispatch(upload_to_gallery_begin());
        return fetch( Ip + "/api/schedule_package/current/" + item.id + "/upload/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
            body: JSON.stringify(item),
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(upload_to_gallery_success())
                return json;
            })
            .catch(error => {
                dispatch(upload_to_gallery_failure())
            });
    }
}

export const upload_to_gallery_begin = () => {
    return {
        type: UPLOAD_TO_GALLERY_BEGIN
    }
}
export const upload_to_gallery_failure = () => {
    return {
        type: UPLOAD_TO_GALLERY_FAILURE
    }
}
export const upload_to_gallery_success = () => {
    return {
        type: UPLOAD_TO_GALLERY_SUCCESS
    }
}

// **************** open gallery collection ******************
export const gallery_view = (item: any) => {
    return dispatch => {
        console.log( Ip + "/api/schedule_package/current/" + item.id + "/uploads/")
        dispatch(fetch_gallery_begin());
        return fetch( Ip + "/api/schedule_package/current/" + item.id + "/uploads/", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_gallery_success(json))
                return json;
            })
            .catch(error => {
                dispatch(fetch_gallery_failure())
            });
    }
}

export const fetch_gallery_begin = () => {
    return {
        type: FETCH_GALLERY_BEGIN
    }
}
export const fetch_gallery_failure = () => {
    return {
        type: FETCH_GALLERY_FAILURE
    }
}
export const fetch_gallery_success = (json) => {
    return {
        type: FETCH_GALLERY_SUCCESS,
        data: json
    }
}
// ************* individual gallery *********************

export const gallery_individual = (item: any) => {
    return dispatch => {
        console.log( Ip + "/api/schedule_package/current/" + item.id + "/uploads/" + item.userId)
        dispatch(gallery_individual_begin());
        return fetch( Ip + "/api/schedule_package/current/" + item.id + "/uploads/" + item.userId, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(gallery_individual_success(json))
                return json;
            })
            .catch(error => {
                dispatch(gallery_individual_failure())
            });
    }
}

export const gallery_individual_begin = () => {
    return {
        type: GALLERY_INDIVIDUAL_BEGIN
    }
}
export const gallery_individual_failure = () => {
    return {
        type: GALLERY_INDIVIDUAL_FAILURE
    }
}
export const gallery_individual_success = (json) => {
    return {
        type: GALLERY_INDIVIDUAL_SUCCESS,
        data: json
    }
}

// ***************** Booking Info ***********************
export const fetch_booking_info = (data) => {
    return dispatch => {
        console.log( Ip + "/api/schedule_package/"+data.id+"/book")
        dispatch(fetch_booking_info_begin());
        return fetch( Ip + "/api/schedule_package/"+data.id+"/book", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'AUTHORIZATIONKEY': token
            },
        })
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetch_booking_info_success(json))
                return json;
            })
            .catch(error => {
                console.log(error)
                dispatch(fetch_booking_info_failure())
            });
    }
}

export const fetch_booking_info_begin = () => {

    return {
        type: FETCH_BOOKING_INFO_BEGIN
    }
}
export const fetch_booking_info_failure = () => {
    return {
        type: FETCH_BOOKING_INFO_FAILURE
    }
}
export const fetch_booking_info_success = (json) => {
    return {
        type: FETCH_BOOKING_INFO_SUCCESS,
        data: json
    }
}